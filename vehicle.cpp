/**
* \file vehicle.cpp
* \brief contains vehicles class definition for cars and bike
*
* This class contains the main implementation of the vehicles.
* It constructs vehicle according to type specified
* The class also handles movement and boundaries collision and riding mechanism
*
* \author Samir Elkhatib
*/

#include "vehicle.h"

/**
 * \brief vehicle::vehicle
 * Constructs the vehicle according to its specified type
 * and starts a timer to handle collision with player
 * \param gameScene, parent scene which the object belongs to
 * \param type, 0 for bike, 1 for student car, 2 for manager car
 * \param x, position x
 * \param y, position y
 * \param scale, vehicle image scaling
 * \param road, road object for boundaries detection
 * \param parent
 */
vehicle::vehicle(QGraphicsScene * gameScene, int type, int x , int y, double scale, QGraphicsItem* road, QObject *parent) : QObject(parent)
{
    scene=gameScene;
    this->road = road;
    this->scale = scale;
    this->type = type;
    if (type == 0){
        setPixmap((QPixmap(":/images/bike.png").scaled(100*scale,100*scale)));
        increment = 5;
    }else if (type == 1){
        setPixmap((QPixmap(":/images/car.png").scaled(160*scale,50*scale)));
        increment = 10;
    }else if (type == 2){
        setPixmap((QPixmap(":/images/manager/car.png").scaled(160*scale,50*scale)));
        increment = 10;
    }
    setPos(x,y);

    // Timer runs on fast fps (15 ms) to detect when player rides car
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(control()));
    timer -> start(15);

}

/**
 * \brief vehicle::control
 * Main controller for car to detect collision with character.
 * If colliding with character, the character is removed from the scene and the car
 * is now in focus which allows the player to control it
 */
void vehicle::control(){
    if(!(scene->collidingItems(this).isEmpty())){
        student * s= dynamic_cast<student *>(scene -> collidingItems(this).at(0));
        // Check if student object is colliding
        if(s){
            scene->removeItem(s); // removes player
            setFlag(QGraphicsItem::ItemIsFocusable);
            setFocus(); // gives focus to the vehicle
        }
    }
}

/**
 * \brief vehicle::keyPressEvent
 * Manages vehicle movement and detects collision with boundaries
 * Boundaries are based on collision with scene boundaries and with road object
 * Road resembles the object the vehicle can move on
 * On Each key pressed collision is detected with road before actually finalizing the new position
 * If the character doesn't collide with the road, movement isn't applied
 * \param event
 */
void vehicle::keyPressEvent(QKeyEvent *event){
    int xp=x();
    int yp=y();

    double width=(double)scene->width();
    double height=(double)scene->height();
    if (event -> key() == Qt::Key_Right && xp < width-increment){
        setPos(xp + pixmap().width(), yp);
        if(!(road->collidesWithItem(this)))
            setPos(xp, yp);
        else
            setPos(xp + increment, yp);
    } else if (event -> key() == Qt::Key_Left && xp >increment ){
        setPos(xp - increment - pixmap().width(), yp);
        if(!(road->collidesWithItem(this)))
            setPos(xp, yp);
        else
            setPos(xp - increment, yp);
    }
    else if (event -> key() == Qt::Key_Up && yp > increment){
        setPos(xp, yp -increment - pixmap().height());
        if(!(road->collidesWithItem(this)))
            setPos(xp, yp);
        else
            setPos(xp, yp -increment);
    } else if (event -> key() == Qt::Key_Down && yp < height-increment){
        setPos(xp , yp+increment + pixmap().height());
        if(!(road->collidesWithItem(this)))
            setPos(xp, yp);
        else
            setPos(xp , yp+increment);
    }
}

/**
 * \brief vehicle::setIncrement
 * Sets vehicle speed
 * \param inc, new vehicle speed
 */
void vehicle::setIncrement(int inc){
    increment = inc;
}

/**
 * \brief vehicle::setScale
 * Sets pixmap according to given scale and type
 * \param scale, new multiplier
 */
void vehicle::setScale(double scale){
    if (type == 0){
        setPixmap((QPixmap(":/images/bike.png").scaled(100*scale,100*scale)));
    }else if (type == 1){
        setPixmap((QPixmap(":/images/car.png").scaled(160*scale,50*scale)));
    }else if (type == 2){
        setPixmap((QPixmap(":/images/manager/car.png").scaled(160*scale,50*scale)));
    }
    this->scale = scale;
}

/**
 * \brief vehicle::getType
 * \return vehicle type (0 for bike, 1 for student car, 2 for manager car)
 */
int vehicle::getType(){
    return type;
}
