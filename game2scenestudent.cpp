/**
* \file game2scenestudent.cpp
* \brief contains student game scene implementation
*
* This class contains the main implementation of the student's game scene.
* It constructs the new scene and creates all relevant elements
*
* \author Samir Elkhatib
* \author Mohammad Agha
*/



#include "game2scenestudent.h"

/**
 * \brief game2scenestudent::game2scenestudent
 * Scene constructor.
 * Creates elements and starts timer
 * \param prev
 */
game2scenestudent::game2scenestudent(PreStartGamePage *prev){
    prevPage=prev;
    mcqDisplayed = false;

    // Set game background
    setBackgroundBrush(QBrush(QImage(":/images/student/bedroom.png").scaled(908, 512)));

    // Set window size
    setSceneRect(0,0,908,512);

    // Set sticky focus to prevent player from clearing focus upon mouse click
    setStickyFocus(true);

    road = new QGraphicsPixmapItem();
    overlay = new QGraphicsPixmapItem();
    player = new student(1,0, height() - 100, this, road,0);


    // Call initializing function
    init();

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    connect(timer, SIGNAL(timeout()), this, SLOT(decrementTimer()));
    timer->start(15);
}

/**
 * \brief game2scenestudent::gameEnd
 * Displays all game end results
 * Constructs end scene from given values collected through the game
 * End scene is displayed as an overlay to the current game scene
 * The function requires a global "values" array from which it takes
 * the collected values
 */
void game2scenestudent::gameEnd()
{
    // Overlaying grey rectangle
    QGraphicsRectItem *endBG = new QGraphicsRectItem();
    endBG->setRect(0,0,width(),height());
    endBG->setBrush(QBrush(QColor(100, 100, 100, 200)));
    addItem(endBG);

    // Font element
    QFont titleFont;
    titleFont.setPixelSize(30);

    // Set title according to player's result
    QGraphicsTextItem *endText = new QGraphicsTextItem();
    endText->setDefaultTextColor(Qt::white);
    endText->setFont(titleFont);
    // Stop the timer and detect if player on time to display appropriate text
    disconnect(timer, SIGNAL(timeout()), this, SLOT(decrementTimer()));
    if(time==0)
        endText->setPlainText("You right on time!!");
    else if(time<0)
        endText->setPlainText("You are Late by "+QString::number(-1*time,10)+" minutes!");
    else
        endText->setPlainText("You are early by "+QString::number(time,10)+" minutes!");
    endText->setPos((width()-endText->boundingRect().width())/2, height()/10);
    addItem(endText);


    QString goodV = "";
    QString h="";
    //QString badV = "";
    // Initialize the good values string
    for(int i = 0; i < 20; i++){
        if (playerValues[i] > 0){
            goodV = goodV + values[i] + "\n\t\t";
            h+=values[i]+" ";
        }
    }
    //if (goodV == "") goodV = "No Good Values \n :(";

    // Initialize the bad values string
//    for(int i = 10; i < 20; i++){
//        if (playerValues[i] > 0){
//            badV = badV + values[i] + "\n";
//        }
//    }
//    if (badV == "") badV = "No Bad Values! \n :)";

    //save to History
    saveToHistory(h);
    // Good values text item
    QGraphicsTextItem *goodText = new QGraphicsTextItem();
    goodText->setPlainText("After doing our thorough statistical analysis on your\nanswers, we found you have thefollowing values: \n\t\t" + goodV);
    goodText->setDefaultTextColor(Qt::yellow);
    titleFont.setPixelSize(20);
    goodText->setFont(titleFont);
    goodText->setPos((width()*0.5)-goodText->boundingRect().width()/2, height()*.3);
    addItem(goodText);

//    // Bad values text item
//    QGraphicsTextItem *badText = new QGraphicsTextItem();
//    badText->setPlainText("Bad Values Collected: \n" + badV);
//    badText->setDefaultTextColor(Qt::red);
//    titleFont.setPixelSize(20);
//    badText->setFont(titleFont);
//    badText->setPos(width() - width()/10 - badText->boundingRect().width(),
//                      height()*.6+ 10);
//    addItem(badText);

    // Back to main menu text
    QGraphicsTextItem *backT = new QGraphicsTextItem();
    backT->setPlainText("Press Spacebar to Go Back To The Menu");
    titleFont.setPixelSize(20);
    backT->setFont(titleFont);
    backT->setDefaultTextColor(Qt::white);
    backT->setPos(width()/2 - backT->boundingRect().width()/2, height()-100);
    addItem(backT);

}
/**
 * \brief game2scenestudent::saveToHistory
 * This fucntion creates a history file if it doesnt exist
 * writes to it or appends  the score related data to be used later
 * \param v string that holds the values attibutes to the player
 */
void game2scenestudent::saveToHistory(QString v)
{
    QString username=prevPage->gamePage->accountPage->nameLabel->text();
    QString filePath=QDir::currentPath()+"/History/"+username+".txt";

    if(!QDir("History").exists()){
        QDir().mkdir("History");
    }
    QFile file(filePath);
    if(QFileInfo(filePath).exists()){
        if(!file.open(QIODevice::Append)){
            qDebug()<<"Could not open file "+filePath;
            return;
        }
    }
    else{
        if(!file.open(QIODevice::WriteOnly)){
            qDebug()<<"Could not open file "+filePath;
            return;
        }
    }
    QDataStream out(&file);
    out.setVersion(QDataStream::Qt_5_9);
    out<< QString("game2")<<QString("Student");
    out<<v<<time;
    file.flush();
    file.close();
}
/**
 * \brief game2scenestudent::init
 * Initializes all scene objects and sets the status to 0
 * Initializers are implemented here to reduce code from constructor
 */
void game2scenestudent::init(){
    road->setPos(0,0);
    overlay->setPos(0,0);
    addItem(road);
    addItem(overlay);
    addItem(player);
    gameTimer =new QGraphicsTextItem("Minutes till class: "+QString::number(60,10));
    gameTimer->setPos(width()*0.77,height()*0.05);
    QFont *newFont=new QFont();
    newFont->setBold(true);
    gameTimer->setFont(*newFont);
    addItem(gameTimer);
    time=60;

    setStatus(0);
}

/**
 * \brief game2scenestudent::decrementTimer
 * Decrements the game timer and sets its text value each second
 * Function is called every 15 ms from timer signal
 * It waits for 67 calls to decrement the timer since it is called every 15 ms
 */
void game2scenestudent::decrementTimer()
{
    int static count=0;
    count++;
    if(count==67){
        time--;
        if(time>=0){
            gameTimer->setPlainText("Minutes till class: "+QString::number(time%60,10));
        }
        count=0;
    }
}


/**
 * \brief game2scenestudent::update
 * Main game engine to detect character collisions an status updates and movement
 * It depends on the status number to implement functionality according to every scene
 */
void game2scenestudent::update(){

    int x[]={1};

    QGraphicsItem *fItem = focusItem(); // get item in focus
    if(pageStatus==1||pageStatus==2){
        // on status 1 and 2, check if character wants to leave room or answer the question
        //static bool beenHere=false;

        student * s= dynamic_cast<student *>(focusItem());
        if(s){
            if(!beenHere[0]&&s->x()<(width()-s->pixmap().width())*.4){
                currentMCQ->deleteMcq();
                int y[]={2};
                int z[]={3};
                currentMCQ=new mcq(false,this,"Are you sure you want to clean your bed and help your mom\n when you are already late? What is more important?","Yes","No",Qt::white,Qt::black,playerValues,z,x,1,y,x,1,1);
                mcqDisplayed=true;
                beenHere[0]=true;
            }else if(!beenHere[0]&&!mcqDisplayed&&s->x()>width()-10-s->pixmap().width()){
                currentMCQ->commitResult(1);
                //mcqDisplayed=false;
                setStatus(3);
            }else if(beenHere[0]&&!mcqDisplayed&&s->x()>width()-10-s->pixmap().width()){
                setStatus(3);
            }

        }

    }
    else if (pageStatus == 3||pageStatus==4){
        // on status 3 and 4, check if vehicle leaves scene to go to next scene
        vehicle * v= dynamic_cast<vehicle *>(focusItem());
        //bool static beenHere=false;
        if(v){
            if(!beenHere[1]){
                qDebug()<<"In vehicle choosing";
                currentMCQ->commitResult(v->getType());
                beenHere[1]=true;
            }

            if (beenHere[1]&&v->x() > width()-10-v->pixmap().width()){
                qDebug()<<"Leaving scene";
                setStatus(5);
            }
        }
    } else if(pageStatus == 5 || pageStatus == 6){
        // on status 5 and 6, check where player is on road to display specific MCQ
            vehicle * v= dynamic_cast<vehicle *>(focusItem());
            //bool static beenHere=false;
            //bool static beenHere2=false;
            //bool static beenHere3=false;
        if(v){
            if (fItem->x() > 130 && fItem->y() > height()/2 && !mcqDisplayed&&!beenHere[2]){
                mcqDisplayed = true;
                int y[]={6};
                int z[]={5};
                int v[]={0};
                currentMCQ=new mcq(true,this,"There is a friend in that house that wants to ride with you\n to college. Since he is a friend and will be late if you don't\n help him, would you be a good friend and give him a ride?!","","",Qt::transparent,Qt::black,playerValues,z,v,1,y,x,1,5);

                // Display MCQ
            } else if (fItem->x() > 300 && fItem->y() > height()/2 - 200 && mcqDisplayed&&!beenHere[2] ){
                mcqDisplayed = false;
                beenHere[2]=true;
                currentMCQ->commitResult(1);
                // Destroy MCQ
            }
            else if(fItem->x() > 130 && fItem->y() < height()*0.3&&!beenHere[2]){
                mcqDisplayed = false;
                beenHere[2]=true;
                currentMCQ->deleteMcq();
                int y[]={6};
                int z[]={5};
                int v[]={0};
                currentMCQ=new mcq(false,this,"So, you decided to take your friend for a ride?","Yes","No",Qt::transparent,Qt::black,playerValues,z,v,1,y,x,1,5);
            }
            if (fItem->x() > 600 && fItem->y() > height()*.4 && !mcqDisplayed&&!beenHere[3]){
                mcqDisplayed = true;
                beenHere[3]=true;
                int y[]={3};
                int z[]={5};
                int v[]={0};
                currentMCQ=new mcq(true,this,"There is an Ice Cream truck up ahead. Do you want to have one?","","",Qt::transparent,Qt::black,playerValues,z,v,1,y,x,1,5);
                // Display MCQ
            }
            else if(fItem->x() > 700&& fItem->y() > height()*.4 &&mcqDisplayed&&!beenHere[4]){
                mcqDisplayed=false;
                beenHere[4]=true;
                currentMCQ->commitResult(1);
            }
            else if(fItem->x() > 600&& fItem->y() < height()*0.3 &&mcqDisplayed&&!beenHere[4]&&beenHere[3]){
                mcqDisplayed=false;
                beenHere[4]=true;
                currentMCQ->deleteMcq();
                int y[]={3};
                int z[]={5};
                int v[]={0};
                currentMCQ=new mcq(false,this,"Take it fast!!Time is Ticking","Yes","No",Qt::transparent,Qt::black,playerValues,z,v,1,y,x,1,5);

            }
            if (fItem -> x() > width()-50){
                // When player leaves scene set status to 7
                setStatus(7);
            }
        }
    } else if (pageStatus == 7){
        // on status 7 wait for player to go to building to set scene to 8
        student * s= dynamic_cast<student *>(focusItem());
        if(s){

        if (fItem -> x() > width()/2 + 70){
            currentMCQ->deleteMcq();
            setStatus(8);
        }
        }
    } else if (pageStatus == 8){
        // on status 8 check when player goes buttom or right
        // if right go to status 9
        // if buttom go to status 10
        student * s= dynamic_cast<student *>(focusItem());
        if(s){
            if(!mcqDisplayed){
                mcqDisplayed=true;
                int y[]={7};
                int z[]={8};
                currentMCQ=new mcq(true,this,"You see the teacher's office opened. You peeked and found the exam papers for tomorrow's Quiz.\nYou can take snapshots(by moving down) or just forget about and go to class(by moving to right).","","",Qt::white,Qt::black,playerValues,z,x,1,y,x,1,5);
            }
            if (s -> x() > width()-70){
                if(mcqDisplayed){
                    mcqDisplayed=false;
                    currentMCQ->commitResult(1);
                }
                disconnect(timer, SIGNAL(timeout()), this, SLOT(decrementTimer()));
                setStatus(9);
            } else if (s ->y() > height()/2 + 70){
                if(mcqDisplayed){
                    mcqDisplayed=false;
                    currentMCQ->deleteMcq();
                }
                setStatus(10);
            }
        }
    }else if(pageStatus == 11){
        // on status 11, wait for player to go right and set status 9
        student * s= dynamic_cast<student *>(focusItem());
        if(s){
            if(!mcqDisplayed){
                mcqDisplayed=true;
                int y[]={9};
                int z[]={10};
                currentMCQ=new mcq(true,this,"Hurry and go to class now!!(To the right)","","",Qt::white,Qt::black,playerValues,z,x,1,y,x,1,0);
            }
            if (s -> x() > width()-200){
                if(mcqDisplayed){
                    mcqDisplayed=false;
                    currentMCQ->deleteMcq();
                }
                disconnect(timer, SIGNAL(timeout()), this, SLOT(decrementTimer()));
                setStatus(9);
            }

        }

    }
}


/**
 * \brief game2scenestudent::setStatus
 * Scene initializer for all game scenes
 * On each new call, set player scale and position, scene background image, scene road,
 * scene overlay, and any other neccessary elements for the scene that need to be initialized
 * or deleted from the previous status. Each scene has its own images
 *
 * Status table:
 * Status 00: In bed, alarm movement
 * Status 01: Alarm stopped, character created and asked to go to bed or right
 * Status 02: Complimentary for status 1
 * Status 03: Asked to pick car or bike after leaving home
 * Status 04: Complimentary for status 3
 * Status 05: Movement on the road with two options: going to friend house and going to ice cream
 * Status 06: Complimentary to status 5
 * Status 07: Outside Engineering Department
 * Status 08: Inside university hallway, asked to go to teacher's office or to class
 * Status 09: Static Image: classroom, calls game end
 * Status 10: Static Image: Teacher's Office
 * Status 11: Complimentary status, goes back to hallway
 *
 *
 * \param newS, new status
 */
void game2scenestudent::setStatus(int newS){
    if (newS == 0){
        road->setPixmap(QPixmap(":/images/student/bedroomFloor.png").scaled(908, 512));
        overlay->setPixmap(QPixmap(":/images/student/bedroomOverlay.png").scaled(908, 512));
        player->setPos(-300, -300);
        a=new alarm(this);


        //MCQ asking if he wants to wake up
        int x[]={1};
        int y[1]={0};
        currentMCQ=new mcq(false,this,"You slept late studying and now you are sleepy, \nbut you need to go to college. Will you wake up now?","Yes","No",Qt::white,Qt::black,playerValues,x,x,1,y,x,1,0);


    } else if(newS == 1){
        road->setPixmap(QPixmap(":/images/student/bedroomFloor.png").scaled(908, 512));
        overlay->setPixmap(QPixmap(":/images/student/bedroomOverlay.png").scaled(908, 512));
        player->setScale(2.5);
        player->setPos(width()/2, height()/2 -90);
        a->stopPosition();
        int x[]={1};
        int y[]={2};
        int z[]={3};
        currentMCQ=new mcq(true,this,"Should you leave without tiding your bed? Should you leave it for your mom to do it for you?\nIf you want to leave, keep moving to the right.","","",Qt::white,Qt::black,playerValues,z,x,1,y,x,1,1);
        // MCQ asking if he wants to fix his bed

    } else if (newS == 2){//put here on purpose
        mcqDisplayed=false;
        // MCQ saying that he has to move right to goout of building

    } else if (newS == 3){
        removeItem(a);
        delete a;
        gameTimer->setPos(width()*0.05,height()*0.9);
        setBackgroundBrush(QBrush(QImage(":/images/student/preScene2.png").scaled(908, 512)));
        overlay->setPixmap(QPixmap(":/images/student/bedroomOverlay.png"));
        road->setPixmap(QPixmap(":/images/student/preScene2Road.png").scaled(908, 512));
        player->setScale(0.8);
        player->setPos(200, height()/2 -200);
        // arguments: scene, type (bike/car), x, y, scale, road
        car = new vehicle(this, 1, 350, height()/2, 0.8, road);
        addItem(car);
        bike = new vehicle(this, 0, 350, height()/2 -140, 0.8, road);
        addItem(bike);

        // MCQ saying that he has to choose bike or car
        int x[]={1};
        int y[]={4};
        int z[]={5};
        currentMCQ=new mcq(true,this,"Choose the Bike to save some money and be environment friendly\n or take the Car and reach faster to the destination?","","",Qt::white,Qt::black,playerValues,z,x,1,y,x,1,3);


    }else if(newS==4){


    }else if (newS == 5){
        setBackgroundBrush(QBrush(QImage(":/images/student/scene2.png").scaled(908, 512)));
        overlay->setPixmap(QPixmap(":/images/student/bedroomOverlay.png"));
        road->setPixmap(QPixmap(":/images/student/scene2Road.png").scaled(908, 512));
        // Remove unused vehicle from scene and destroy it
        vehicle * v= dynamic_cast<vehicle *>(focusItem());
        if(v){
            if(v->getType() == 0){
               // we have bike
               removeItem(car);
               delete car;
               v->setIncrement(3);
            }else{
                removeItem(bike);
                delete bike;
                v->setIncrement(6);
            }
            v->setScale(0.3);
            v->setPos(20, height()/2 + 60);
        }

    }
    else if(newS==6){

    }else if (newS==7){
        gameTimer->setPos(width()*0.05,height()*0.05);
        setBackgroundBrush(QBrush(QImage(":/images/student/engDepOutside.png").scaled(908, 512)));
        overlay->setPixmap(QPixmap(":/images/student/bedroomOverlay.png"));
        road->setPixmap(QPixmap(":/images/student/engDepOutsideFloor.png").scaled(908, 512));
        // Remove car and delete it
        QGraphicsItem * v = focusItem();
        removeItem(v);
        delete(v);
        // Add player again
        addItem(player);
        player->setPos(20, height()/2);
        player->setFocus();
        int x[]={1};
        currentMCQ=new mcq(true,this,"Hurry and enter the department!","","",Qt::white,Qt::black,playerValues,x,x,1,x,x,1,7);

    } else if (newS == 8){
        gameTimer->setPos(width()*0.8,height()*0.05);
        setBackgroundBrush(QBrush(QImage(":/images/student/departmentHallway.png").scaled(908, 512)));
        overlay->setPixmap(QPixmap(":/images/student/bedroomOverlay.png"));
        road->setPixmap(QPixmap(":/images/student/departmentHallwayFloor.png").scaled(908, 512));
        player->setPos(width()/2, height()/2 - 50);
        player -> setScale(2);
        player->setFocus();
    } else if(newS == 9){
        setBackgroundBrush(QBrush(QImage(":/images/student/class.jpg").scaled(908, 512)));
        overlay->setPixmap(QPixmap(":/images/student/bedroomOverlay.png"));
        road->setPixmap(QPixmap(":/images/student/bedroomOverlay.png"));
        player->setPos(-300, -300);
        gameEnd();
    } else if(newS == 10){
        gameTimer->setPos(width()*0.04,height()*0.05);
        setBackgroundBrush(QBrush(QImage(":/images/student/teacherOffice.jpg").scaled(908, 512)));
        overlay->setPixmap(QPixmap(":/images/student/bedroomOverlay.png"));
        road->setPixmap(QPixmap(":/images/student/bedroomOverlay.png"));
        player->setPos(-300, -300);
        int x[]={1};
        int y[]={7};
        int z[]={8};
        currentMCQ=new mcq(false,this,"Are you sure you want to risk it and cheat?\n ","Yes","No",Qt::white,Qt::black,playerValues,z,x,1,y,x,1,10);
        mcqDisplayed=false;

    } else if(newS==11){
        gameTimer->setPos(width()*0.8,height()*0.05);
        setBackgroundBrush(QBrush(QImage(":/images/student/departmentHallway.png").scaled(908, 512)));
        overlay->setPixmap(QPixmap(":/images/student/bedroomOverlay.png"));
        road->setPixmap(QPixmap(":/images/student/departmentHallwayFloor.png").scaled(908, 512));
        player->setPos(width()/2, height()/2 - 50);
        player -> setScale(2);
        player->setFocus();
    }


    // Set pageStatus to the new status
    pageStatus = newS;
}
