/**
* \file mcq.cpp
* \brief contains multiple choice questions main implementation
*
* This class contains the main implementation of the MCQs
*
* \author Mohammad Agha
*/
#include "mcq.h"

/**
 * \brief mcq::mcq
 * initializes the MCQ and its text elements
 * \param questionOnly
 * \param scene parent scene
 * \param question true if question only, false if there are options
 * \param answer1, first question
 * \param answer2, second question
 * \param backgroundColor, background color
 * \param fontColor, text color
 * \param playerValues, global values array
 * \param valueIdA, first value (positive value)
 * \param valueChangeA, number of additions (usually given 1)
 * \param sizeA, valueId array size
 * \param valueIdB, second value (negative value)
 * \param valueChangeB, number of additions
 * \param sizeB, valueId array size
 * \param curretnScene, current scene status given, goes to next status upon answering
 */
mcq::mcq(bool questionOnly,QGraphicsScene *scene,QString question,QString answer1,QString answer2,Qt::GlobalColor backgroundColor,Qt::GlobalColor fontColor,int *playerValues,int* valueIdA,int* valueChangeA,int sizeA,int* valueIdB,int* valueChangeB,int sizeB,int curretnScene)
{
    this->currentScene=curretnScene;
    this->sizeA=sizeA;
    this->valueIdA=new int[sizeA];
    this->valueChangeA=new int[sizeA];
    for(int i =0;i<sizeA;i++){
        this->valueIdA[i]=valueIdA[i];
        this->valueChangeA[i]=valueChangeA[i];
    }

    this->sizeB=sizeB;
    this->valueIdB=new int[sizeB];
    this->valueChangeB=new int[sizeB];
    for(int i =0;i<sizeB;i++){
        this->valueIdB[i]=valueIdB[i];
        this->valueChangeB[i]=valueChangeB[i];
    }


    this->color=fontColor;
    this->questionOnly=questionOnly;
    this->playerValues=playerValues;
    tAnswerA = new CustomQGraphicsTextItem(answer1,backgroundColor,0);
    tAnswerB = new CustomQGraphicsTextItem(answer2,backgroundColor,1);
    tQuestion=new CustomQGraphicsTextItem(question,backgroundColor,2);
    this->scene=scene;
    createTexts();
    connect(this,SIGNAL(setNextScene(int)),scene,SLOT(setStatus(int)));
    connect(tAnswerB,SIGNAL(endMcq(int)),this,SLOT(commitResult(int)));
    connect(tAnswerA,SIGNAL(endMcq(int)),this,SLOT(commitResult(int)));
    connect(scene,SIGNAL(endMcq(int)),this,SLOT(commitResult(int)));
}

/**
 * \brief mcq::createTexts
 * sets the texts for the mcq question and the chosen colors and options
 */
void mcq::createTexts(){
    QFont titleFont;
    titleFont.setPixelSize(20);
    tQuestion->setDefaultTextColor(color);
    tQuestion->setFont(titleFont);
    //tQuestion->setPlainText("After sleeping late last night studying, the alarm starts ringing to wake you up for \ncollege and each minute you are late will make you late for class. What will you do?");
    tQuestion->setPos((scene->width()-tQuestion->boundingRect().width())*0.5, scene->height()*0.7);
    scene->addItem(tQuestion);
    if(!questionOnly){
        tAnswerA->setDefaultTextColor(color);
        tAnswerA->setFont(titleFont);
        //tAnswer1->setPlainText("Stop the alarm and get ready to go \nto college with a cup of cofee maybe");
        tAnswerA->setPos((scene->width()-tAnswerA->boundingRect().width())*0.1, scene->height()*0.85);
        scene->addItem(tAnswerA);

        tAnswerB->setDefaultTextColor(color);
        tAnswerB->setFont(titleFont);
        //tAnswer2->setPlainText("Continue sleeping because you are tired\n and cannot focus in class anyway");
        tAnswerB->setPos((scene->width()-tAnswerB->boundingRect().width())*0.9, scene->height()*0.85);
        scene->addItem(tAnswerB);
    }
}

/**
 * \brief mcq::commitResult
 * saves according to the selected mcq answer
 * the values are changed according to what the payer picks
 * \param choice selected anser
 */
void mcq::commitResult(int choice){

    // save player's choice if 1
    if(choice){
        for(int i=0;i<sizeA;i++){
            playerValues[valueIdA[i]]+=valueChangeA[i];
            qDebug()<<playerValues[valueIdA[i]];
        }
    }
    else{
        for(int i=0;i<sizeB;i++){
            playerValues[valueIdB[i]]+=valueChangeB[i];
            qDebug()<<playerValues[valueIdB[i]];
        }
    }
    qDebug()<<"Here";

    // move to next scene
    emit setNextScene(++currentScene);

    // remove the mcq
    deleteMcq();
}

/**
 * \brief mcq::deleteMcq
 * removes the MCQ and all its elements
 */
void mcq::deleteMcq(){
    qDebug()<<"In delete";
    delete tAnswerA;
    delete tAnswerB;
    delete tQuestion;
    delete [] valueIdA;
    delete [] valueChangeA;
    delete [] valueIdB;
    delete [] valueChangeB;
    delete this;
}

