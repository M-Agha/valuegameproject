/**
* \file loadingpage.h
* \brief contains the game history implementation
*
* \author Mohamad Agha
*/
#ifndef LOADINGPAGE_H
#define LOADINGPAGE_H

#include <QWidget>
#include <QtWidgets>
#include "account.h"
#include <QVector>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QCategoryAxis>
QT_CHARTS_USE_NAMESPACE
/**
* \file loadingpage.h
* \brief LoadingPage Class
*
* This file has all history componenets
*/
class LoadingPage : public QWidget
{
    Q_OBJECT
public:
    explicit LoadingPage(Account *,QString,QWidget *parent = nullptr);///<Constructor of the history page
    Account * previousPage;///<Pointer to the previous page of the menu
    QLabel * loadingLabel;///<Label to hold the history strings
    QPushButton * backButton;///<Button to go back
    QVBoxLayout * vLayout;///<Vetical layout that holds the items
    void addHistory();///<Fucntion used to Extract the histroy from files and put them in to a chart
    QString handleGame3History(QDataStream *in);///<Fucntion used to Extract the histroy from files with game 3 specific format
    QVector<int> game3Scores;///<Vector to hold the scores of game 3 to be displayed in the chart
    QString handleGame2History(QDataStream *in);///<Fucntion used to Extract the histroy from files with game 2 specific format
    QString handleGame1History(QDataStream *in);///<Fucntion used to Extract the histroy from files with game 1 specific format
    QVector<int> game1Scores;///<Vector to hold the scores of game 1 to be displayed in the chart
    QChartView *chartView;///<Chart view to hold the chart
signals:

public slots:
    void backToPreStartGamePage();///<Fucntion to handle going back to previous page
};

#endif // LOADINGPAGE_H
