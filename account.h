#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <QWidget>
#include <QtWidgets>
#include "login.h"
class Account : public QWidget
{
    Q_OBJECT
public:
    explicit Account(QString name,login *,QWidget *parent = nullptr);
    QLabel * nameLabel;
    login * previousMenu;
    QPushButton * historyButton;
    QPushButton * game1Button;
    QPushButton * game2Button;
    QPushButton * game3Button;
    QPushButton * backButton;
    QVBoxLayout * vLayout;
    void setVerticalLayout();

signals:

public slots:
    void openGame1();
    void openGame2();
    void openGame3();
    void openHistory();
    void back();
};

#endif // ACCOUNT_H

//Comment
