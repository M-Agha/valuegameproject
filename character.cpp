/**
*\file character.cpp
*\brief This class contains character class function implementation and creation
*
*
*This class handles character jumping and falling by usig a timer to incremetally move the character to a high position and back to initial position
*It alse handles character back and forth movement by storing the simeltaneous keys and acting on it on the update fucntion that gets called on a timer timeout.
*\author Mohamad Agha
*/
#include "character.h"
/**
 * \brief Definition of the constructor of the character
 * Constructs the character object
 * set the picture and position of the character and add to scene passed
 * Initialize the timers and start the updating position timer
 * \param gameScene, parent scene which the object belongs to
 * \param parent
 */
character::character(QGraphicsScene * gameScene,QObject *parent) : QObject(parent)
{
    scene=gameScene;
    setPixmap((QPixmap(":/images/game3/Jiren.png")).scaled(70,120));
    setPos(0,(scene->height()-1.5*boundingRect().height()));
    setFlag(QGraphicsItem::ItemIsFocusable);
    setFocus();
    scene->addItem(this);
    timer = new QTimer(this);
    timer2=new QTimer(this);
    connect(timer2, SIGNAL(timeout()), this,SLOT(updatePos()));
    timer2->start(1000/60);
}
/**
*\brief Function that updates the set pressedkeys with the newly pressed buttons.
*/
void character::keyPressEvent(QKeyEvent *event)
{
    pressedKeys += ((QKeyEvent*)event)->key();
}
/**
*\brief Function that removes from the set pressedkeys the recenlty released keys
*/
void character::keyReleaseEvent(QKeyEvent *event)
{
    pressedKeys -= ((QKeyEvent*)event)->key();
}
/**
*\brief Function that updates position of the charater based on the keys stored in the set pressed keys.
*
* The keys up, w and space will trigger a jump which is handled by a timer and the jump function
* The key Left and A will move the character to the left
* The key Right and D will move the character to the right
*/
void character::updatePos()
{
    int xp=x();///<The current x position of the bike.
    if( (pressedKeys.contains(Qt::Key_Left) ||pressedKeys.contains(Qt::Key_A))&& xp>0)
    {
        setX(xp-Move_Speed);
    }
    if((pressedKeys.contains(Qt::Key_Right)||pressedKeys.contains(Qt::Key_D))&&xp<scene->width()-boundingRect().width()){
        setX(xp+Move_Speed);
    }
    if (!inFall&&(pressedKeys.contains(Qt::Key_Space)||pressedKeys.contains(Qt::Key_Up)||pressedKeys.contains(Qt::Key_W)) ){
        connect(timer,SIGNAL(timeout()),this,SLOT(jump()));
        timer->start(Jump_Time);

    }
}
/**
*\brief Function that handles the jump of the character.
*
* This fucntion is called on timer timeout and will wait
* for Jump_Height number of timer timeouts before stoping the jump
* and it will also trigger the function fall to start the falling process
*/
void character::jump()
{
    int static count=0;
    count++;
    setPos(x(),y()-1);
    if(count==Jump_Height){
        timer->stop();
        disconnect(timer,SIGNAL(timeout()),this,SLOT(jump()));
        count=0;
        inFall=true;
        connect(timer,SIGNAL(timeout()),this,SLOT(fall()));
        timer->start(Jump_Time);
    }
}
/**
*\brief Function that handles the fall of the character.
*
* This fucntion is called on timer timeout and will wait
* for Jump_Height number of timer timeouts before stoping the fall
* and it will also allow the start of a possible jump by setting the boolean
* inFall to false
*/
void character::fall()
{

    int static count=0;
    count++;
    setPos(x(),y()+1);
    if(count==Jump_Height){
        timer->stop();
        disconnect(timer,SIGNAL(timeout()),this,SLOT(fall()));
        count=0;
        inFall=false;
    }
}
