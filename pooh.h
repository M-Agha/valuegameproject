/**
* \file pooh.h
* \brief contains implementation of pooh character
*
* \author Samir Elkhatib
*/

#ifndef BIKE_H
#define BIKE_H

#include <QObject>
#include <QKeyEvent>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
//#include "bee.h"
class pooh : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    explicit pooh(QGraphicsScene*,QObject *parent = nullptr);
    void keyPressEvent(QKeyEvent *event);
    QGraphicsScene * scene; // reference to parent scene
    int collectedTraits[20]={0}; // store collected traits
    bool win=false; // check if chaaracter won
signals:
void iWon();
public slots:
};

#endif // BIKE_H
