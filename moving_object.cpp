/**
*\file moving_object.cpp
*\brief This class contains moving_object class function implementation and creation
*
*
* This class handles the moving character and value object movement and destruction
* A timer is used to update the continous movement of the moving object
*\author Mohamad Agha
*/
#include "moving_object.h"
/**
 * \brief Definition of the constructor of the moving object
 * Constructs the moving object
 * Create the values that are attacehd to this object
 * Initialize the timers
 * \param high a boolean used to indicate which lane th object will spawn in
 * \param game3scene, parent scene which the object belongs to
 * \param parent
 */
Moving_Object::Moving_Object(bool high,game3scene * gameScene,QObject *parent) : QObject(parent)
{
    scene=gameScene;
    this->high=high;
    init();
    createValue(scene->values);
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));
    timer->start(1000/60);

}
/**
*\brief Function that initializes the object image by setting it image and initial position.
*/
void Moving_Object::init()
{
    setPixmap((QPixmap(":/images/game3/star.png")).scaled(27,27));
    if(high)
        setPos(scene->width()-boundingRect().width(),(scene->height()-9*boundingRect().height()));
    else
        setPos(scene->width()-boundingRect().width(),(scene->height()-4*boundingRect().height()));
    scene->addItem(this);
    timer = new QTimer(this);
}
/**
*\brief Function that creates the value object that is attached to the moving object.
*
* It is made to be centerd with the moving object and then setting the location of the lane
*/
void Moving_Object::createValue(QString values[])
{
    // Creates values in a biased random way so that bad values are more than good values by 70% to 30% difference
    int word;
    int valueType = rand()%10;

    if (valueType>= 7){
        word = rand() %10;
    }else{
        word = rand()%10 + 10;
    }

        valueObject = new QGraphicsTextItem(values[word]);
        valueId=word;//this is a unique id for the word
        int length=valueObject->boundingRect().width();
        int diff=(length-boundingRect().width())/2;
        if(high)
            valueObject->setPos(scene->width()-length+diff,(scene->height()-8*boundingRect().height()));
        else
            valueObject->setPos(scene->width()-length+diff,(scene->height()-3*boundingRect().height()));
        scene->addItem(valueObject);
}
/**
*\brief Function that updates the position of the moving object and the attached value by timer timeout
*
* Also, this function detects the collision with the character
* It gives this value to the character and deletes itself
* It also deletes itselft on boundary collision
*/
void Moving_Object::move()
{
    if (scene->level == 4){
        setX(x()-6);
        valueObject->setX(valueObject->x()-6);
    } else{
        setX(x()-4);
        valueObject->setX(valueObject->x()-4);
    }


    if(this->collidesWithItem(scene->chr)){

        scene->chr->collectedTraits[valueId]++;
        if(valueId <= 9){
            scene->score+=10;
        }else
            scene->score-=10;
        scene->removeItem(valueObject);
        delete valueObject;
        scene->removeItem(this);
        delete this;
    }
    else if (x() < -1*boundingRect().width()){

        scene->removeItem(valueObject);
        delete valueObject;
        scene->removeItem(this);
        delete this;
    }
}
/**
*\brief Function that stops the timer which will stop the movement of the moving object along with the values object
*/
void Moving_Object::timerKill()
{
    timer->stop();
}
