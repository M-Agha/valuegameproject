/**
* \file alarm.h
* \brief contains alarm object definition
*
* This class contains the main implementation of the alarm and its animation.
*
* \author Mohammad Agha
*/

#ifndef ALARM_H
#define ALARM_H

#include <QObject>
#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QGraphicsScene>

class alarm :public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    alarm(QGraphicsScene *, QObject *parent = nullptr);
    QGraphicsScene * scene;
    QTimer *timer; // animation timer
    int upOrDown=0; // position status
    void stopPosition(); // stops animation
signals:

public slots:
    void alarmJump(); // animates alarm

};

#endif // ALARM_H
