#include "account.h"
#include "game.h"
#include "loadingpage.h"

Account::Account(QString name,login *previousPage,QWidget *parent) : QWidget(parent)
{
    setWindowTitle("Account Menu");
    previousMenu=previousPage;
    nameLabel=new QLabel(name);
    nameLabel->setAlignment(Qt::AlignCenter);
    historyButton=new QPushButton("History");
    game1Button=new QPushButton("Game 1");
    game2Button=new QPushButton("Game 2");
    game3Button=new QPushButton("Game 3");
    backButton=new QPushButton("Log out");
    vLayout =new QVBoxLayout();
    setVerticalLayout();
    setLayout(vLayout);
    connect(game1Button, SIGNAL(clicked()), this, SLOT(openGame1()));
    connect(game2Button, SIGNAL(clicked()), this, SLOT(openGame2()));
    connect(game3Button, SIGNAL(clicked()), this, SLOT(openGame3()));
    connect(backButton, SIGNAL(clicked()), this, SLOT(back()));
    connect(historyButton,SIGNAL(clicked()),this,SLOT(openHistory()));
}
void Account::setVerticalLayout(){
    vLayout->addWidget(nameLabel);
    vLayout->addWidget(game1Button);
    vLayout->addItem(new QSpacerItem(220, 10));
    vLayout->addWidget(game2Button);
    vLayout->addItem(new QSpacerItem(220, 10));
    vLayout->addWidget(game3Button);
    vLayout->addItem(new QSpacerItem(220, 10));
    vLayout->addWidget(historyButton);
    vLayout->addItem(new QSpacerItem(180, 10));
    vLayout->addWidget(backButton);
}
void Account::openGame1(){
    Game * gamePage=new Game(game1Button->text(),this);
    this->hide();
    gamePage->show();
}
void Account::openGame2(){
    Game * gamePage=new Game(game2Button->text(),this);
    this->hide();
    gamePage->show();
}
void Account::openGame3(){
    Game * gamePage=new Game(game3Button->text(),this);
    this->hide();
    gamePage->show();
}

void Account::openHistory()
{
    LoadingPage *p=new LoadingPage(this,"History");
    this->hide();
    p->show();
}

void Account::back(){
    //login *l = new login();
    //l -> show();
    previousMenu->show();
    this -> close();
}
