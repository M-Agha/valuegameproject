/**
* \file prestartgamepage.h
* \brief implementation for the level selection page
* last page before game start
*
* \author Samir Elkhatib
* \author Mohammad Agha
*/
#ifndef PRESTARTGAMEPAGE_H
#define PRESTARTGAMEPAGE_H

#include <QWidget>
#include <QtWidgets>
#include "game.h"


class PreStartGamePage : public QWidget
{
    Q_OBJECT
public:
    explicit PreStartGamePage(int gameNum,Game*,QWidget *parent = nullptr);
    Game * gamePage;
    QLabel *selectLevelLabel, *instructionLabel; // labels used
    // buttons used
    QPushButton * level1Button;
    QPushButton * level2Button;
    QPushButton * level3Button;
    QPushButton * level4Button;
    QPushButton * backButton;
    QVBoxLayout *vLayout;
    int gameID; // game id to specify which game is chosen
    void setWinLayout();
    void startG(QGraphicsScene *);
signals:

public slots:
    void startGame1Easy();
    void startGame1Medium();
    void startGame1Hard();
//    void startGame();
    void startStudent();
    void startManager();
    void backToGamePage();
    void startGameEasy();
    void startGameMedium();
    void startGameHard();
    void startGameInsane();
};

#endif // PRESTARTGAMEPAGE_H
