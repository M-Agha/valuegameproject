/**
* \file game2scenemanager.h
* \brief contains manager game scene implementation
*
* \author Samir Elkhatib
* \author Mohammad Agha
*/

#ifndef GAME2SCENEMANAGER_H
#define GAME2SCENEMANAGER_H

#include <QGraphicsScene>
#include "prestartgamepage.h"
#include "student.h"
#include "vehicle.h"
#include "mcq.h"
#include <QGraphicsPixmapItem>
#include <QTimer>
#include "alarm.h"

class game2scenemanager : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit game2scenemanager(PreStartGamePage *);
    PreStartGamePage *prevPage; // Games menu pointer
    student *player; // Player pointer
    vehicle *car; // vehicle pointer
    QGraphicsTextItem *gameTimer; // Timer text item
    int time; // Current time left for player
    // Values passed as a global variable
    QString values[20]={"Energetic", "Lazy", "Helpful", "Ignorant", "Environmental Friendly",
        "Hasty", "Friendly", "Cheater", "Honest", "Commited",
        "Negligent","Family Work Balance", "Caring", "Trustworhty", "Corrupt",
        "Humble", "Overbearing", "Aggressive", "Cruel", "Mean"};
    int playerValues[20]={0}; // list of values acquired by player
    mcq * currentMCQ; // current MCQ
    int pageStatus; // Scene current status. Controls what is displayed on the scene
    void gameEnd(); // Ends game and displays results
    QGraphicsRectItem *endBG; // End scene background
    alarm *a; // Alarm object
    bool beenHere [7]={false};
    void saveToHistory(QString v);

    bool mcqDisplayed; // check if MCQ displayed


    QGraphicsPixmapItem *road, *overlay, *wife; // different objects used in the game scene
    /**
     * road: main road object for collision detection
     * overlay: used to display items like banana peel
     * wife: wife object in first scene
     */

    QTimer *timer; // main timer, called every 15 ms

    void init();
protected:
    /**
     * \brief mouseReleaseEvent
     * Checks when mouse is clicked
     * \param event
     */
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
    {
        QGraphicsScene::mousePressEvent(event);
    }
    /**
     * \brief keyPressEvent
     * Checks when keyboard buttons are clicked
     * \param event
     */
    void keyPressEvent(QKeyEvent *event)
    {
        QGraphicsScene::keyPressEvent(event);
        if (event -> key() == Qt::Key_A){

            emit endMcq(0);
        }
        else if(event -> key() == Qt::Key_B){

            emit endMcq(1);
        } else if (event ->key() == Qt::Key_Space){
            // When spacebar pressed delete the current view and scene and head back to menu
            QGraphicsView *vi = views().at(0);
            vi->hide();
            delete vi;
            prevPage->show();
            this->clear();
            delete this;
        }

    }

signals:
    void endMcq(int id);

public slots:
    void decrementTimer();
    void update();
    void setStatus(int);

};

#endif // GAME2SCENEMANAGER_H
