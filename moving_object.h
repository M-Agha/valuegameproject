/**
* \file moving_object.h
* \brief Moving_Object Class
*
* This file has the moving object related fields and fucntion declaration
*/
#ifndef MOVING_OBJECT_H
#define MOVING_OBJECT_H

#include "game3scene.h"
#include <QGraphicsPixmapItem>
#include <QTimer>
class Moving_Object :public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    explicit Moving_Object(bool,game3scene*,QObject *parent = nullptr);///<Constructor of the character object
    void init();///<Function used to initialze this object
    void createValue(QString values[]);///<Function used to create a random value and attach it to this object
    QTimer *timer;///<timer used to move this object
    game3scene * scene;///<The scene the characetr is in
    int valueId;///<The valueId based on the values array in game3scene
    bool high=false;///<A boolean used to indicate if this is the high lane or not
    QGraphicsTextItem *valueObject;///<the graphic text object that will show the value
public slots:
    void move();///<Function triggered by timer to move the object
    void timerKill();///<tFunction used to stop the timer and thus stop the object from moving
};

#endif // MOVING_OBJECT_H
