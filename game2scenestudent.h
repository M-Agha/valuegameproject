/**
* \file game2scenestudent.h
* \brief contains student game scene implementation
*
* \author Samir Elkhatib
* \author Mohammad Agha
*/

#ifndef GAME2SCENE_H
#define GAME2SCENE_H

#include <QGraphicsScene>
#include "prestartgamepage.h"
#include "student.h"
#include "vehicle.h"
#include "mcq.h"
#include <QGraphicsPixmapItem>
#include <QTimer>
#include "alarm.h"
class game2scenestudent : public QGraphicsScene
{
    Q_OBJECT
public:
    game2scenestudent(PreStartGamePage *);
    PreStartGamePage *prevPage; // Games menu pointer
    student *player; // Character student
    vehicle *car, *bike; // Two vehicles pointers
    QGraphicsTextItem *gameTimer; // Main timer text item
    int time; // Current time left for player
    // Values, passed as a global variable
    QString values[20]={"Energetic", "Lazy", "Helpful", "Ignorant", "Environmental Friendly",
        "Hasty", "Friendly", "Cheater", "Honest", "Commited",
        "Negligent","Family Work Balance", "Caring", "Trustworhty", "Corrupt",
        "Humble", "Overbearing", "Aggressive", "Cruel", "Mean"};
    int playerValues[20]={0}; // List of values acquired by player
    mcq * currentMCQ; // Current multipl choice question

    int pageStatus; // Scene current status. Controls what is displayed on the scene
    void gameEnd(); // Ends the game and displays results
    QGraphicsRectItem *endBG; // End scene background
    alarm *a; // Alarm instance
    void saveToHistory(QString v);
    bool mcqDisplayed; // Check if MCQ is displayed now
    bool beenHere [5]={false};
    QGraphicsPixmapItem *overlay; // QGraphics Items that are displayed over main image
    QGraphicsPixmapItem *road; // QGraphics Item resembling the road which player can move on

    QTimer *timer; // Main timer, called every 15 ms

    void init(); // Initializing function
protected:
    /**
     * \brief mouseReleaseEvent
     * Checks when mouse is clicked
     * \param event
     */
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
    {
        QGraphicsScene::mousePressEvent(event);
    }
    /**
     * \brief keyPressEvent
     * Checks when keyboard buttons are clicked
     * \param event
     */
    void keyPressEvent(QKeyEvent *event)
    {
        QGraphicsScene::keyPressEvent(event);
        if (event -> key() == Qt::Key_A){
            emit endMcq(0);
        }
        else if(event -> key() == Qt::Key_B){
            emit endMcq(1);
        } else if (event ->key() == Qt::Key_Space){
            // When spacebar pressed delete the current view and scene and head back to menu
            QGraphicsView *vi = views().at(0);
            vi->hide();
            delete vi;
            prevPage->show();
            delete this;
        }

    }

signals:
    void endMcq(int id);

public slots:
    void decrementTimer();
    void update();
    void setStatus(int);
};

#endif // GAME2SCENE_H
