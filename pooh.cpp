/**
* \file pooh.cpp
* \brief contains implementation of pooh character
* the character is implemented along with its keyPressed events
*
* \author Samir Elkhatib
*/
#include "pooh.h"

/**
 * \brief pooh::pooh
 * the main character under control by the user
 * initializer give element focus as well
 * \param gameScene reference to parent scene
 * \param parent
 */
pooh::pooh(QGraphicsScene * gameScene,QObject *parent) : QObject(parent)
{
    scene=gameScene;
    setPixmap((QPixmap(":/images/pooh.png")).scaled(32,38));
    setPos(scene->width()*0.5,scene->height()*0.01);
    setFlag(QGraphicsItem::ItemIsFocusable);
    setFocus();
}

/**
 * \brief pooh::keyPressEvent
 * on key pressed moves pooh and accounts for out of bounds
 * \param event returns pressed key
 */
void pooh::keyPressEvent(QKeyEvent *event){
    int xp=x(); //initial position x
    int yp=y(); //initial position y
    int increment=10; // increment speed
    double width=(double)scene->width();
    double height=(double)scene->height();

    // accouting for boundaries
    if (event -> key() == Qt::Key_Right && xp < width*0.96){
        if (yp<height*0.75){
        setPos(xp + increment, yp);
        }
    } else if (event -> key() == Qt::Key_Left && xp >increment ){

        if (yp<height*0.75){
            setPos(xp - increment, yp);
        }
    } else if (event -> key() == Qt::Key_Up && yp > increment){
            setPos(xp, yp -increment);
    } else if (event -> key() == Qt::Key_Down && (yp < height*0.73||
                                                 (xp>width*0.46&&
                                                  xp<width*0.52&&
                                                  yp<height*0.92) )){
        setPos(xp , yp+increment);
    }

    // emit signal when at specific location
    if(yp> height*0.74){
        emit iWon();
    }
}

