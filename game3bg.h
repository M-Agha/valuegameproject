/**
* \file game3bg.h
* \brief contains the definition of the background objects class for game 3
*
* This header contains the main implementation of the pixmap objects
*
* \author Samir Elkhatib
*/

#ifndef GAME3BG_H
#define GAME3BG_H

#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>

class game3bg : public QObject
{
    Q_OBJECT
public:
    explicit game3bg(QGraphicsScene *, QObject *parent = nullptr);

    void move(int);

private:
    QGraphicsPixmapItem *bg1, *bg2, *r1, *r2; // background and road objects
    QGraphicsScene *scene; // reference to parent scene

signals:

public slots:
};

#endif // GAME3BG_H
