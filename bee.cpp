/**
* \file bee.cpp
* \brief contains implementation of game 1 bees
* deteccts collision with character and updates values as well
* contains creation of values and lanes
*
* \author Samir Elkhatib
* \author Mohammad Agha
*/

#include "bee.h"
#include "pooh.h"
/**
 * \brief bee::bee
 * creates a bee on a specific lane and sets its value and speed accordingly
 * \param values list of values gives
 * \param gameScene parent scene
 * \param lane the lane number which the bee belongs to
 * \param parent
 */
bee::bee(QString values[] , game1scene* gameScene,int lane,QObject *parent) : QObject(parent)
{
    this->lane = lane;
    beeImage.load(":/images/bee.png");
    scen=gameScene;

    // Initialize bee dimentions according to scene
    height = scen->height();
    width = scen->width();
    init();
    setPixmap(QPixmap::fromImage(beeImage).scaled(35.34,25));

    // Initialize value
    this->createValue(values);

    // Signal controls automatic movement
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(move()));
    connect(scen,SIGNAL(toBeeTimer()),this,SLOT(timerKill()));
    timer -> start(15);

}

/**
 * \brief bee::timerKill
 * stops the bee, emited on game end
 */
void bee::timerKill(){
    timer->stop();
}

/**
 * \brief bee::move
 * moves the bee according to the lane it belongs to and to the game level
 * each lane is set to have a particular speed multiplier
 * the total speed is a multiplication of the lane and the max speed
 * the max speed is set according to game level
 */
void bee::move(){

    // List of speeds for each lane
    double speeds[8] = {0.5, 0.8, 1, 0.6, 0.4, 0.7, 0.9, 1};
    double factor = speeds[lane];
    int maxSpeed = 4;

    // maxSpeed depends on level
    if (scen->level == 1){
    } else if (scen->level == 2){
        maxSpeed = 10;
    }else{
        maxSpeed = 30;
    }

    // Speed depends on lane
    setPos(x() + factor*maxSpeed*direction,y());
    value->setPos(value->x() + factor*maxSpeed*direction, y());

    // Collision and out of bounds detection
    if (x() > width + 100 ||x() < -100){
        scen->removeItem(value);
        delete value;
        scen->removeItem(this);
        delete this;
    }
    else if(!(scen -> collidingItems(this).isEmpty())){
        // check if colliding with pooh
        pooh * p= dynamic_cast<pooh *>(scen -> collidingItems(this).at(0));
        if(p){
            scen->player->collectedTraits[valueID]++;
            if(valueID>9){//bad value
                // decrease heart
                scen->updateLives(false);
            }
            else {//good value
                //increase heart
                scen->updateLives(true);
            }

            // remove bee and corresponding value
            scen->removeItem(value);
            delete value;
            scen->removeItem(this);
            delete this;
        }else{
            // in case bee collides with another bee upon creation
            scen->removeItem(value);
            delete value;
            scen->removeItem(this);
            delete this;
        }
    }
}

/**
 * \brief bee::init
 * initialize the bee and its value
 * set initial position and direction according to lane number
 * set whether the bee is a good or bad value
 */
void bee::init(){

    // Upper lanes
    if (lane < 4){
        setPos(-40, lane*height/12.8 + height*0.11);

        // Positive direction (left to right)
        direction = 1;
    } else{
        setPos(width + 40, lane*height/13.45 + height*0.11);

        // Negative direction (right to left)
        direction = -1;
        // Flip the bee image
        beeImage = beeImage.mirrored(true, false);
    }
    while (!this->collidingItems().isEmpty()){
        setPos(x() - 10*direction, y());
    }
}

/**
 * \brief bee::createValue
 * create the bee's correspoding value randomly
 * the random is biased to bad values to make game harder
 * \param values
 */
void bee::createValue(QString values[]){
    int type = rand() % 10; // specifies good or bad
    int word = rand() %10; // randomly pick from bad or good list
    if(type > 6) {
        value = new QGraphicsTextItem(values[word]);
        valueID=word;
        //this is a unique id for the word
    }else {
        value = new QGraphicsTextItem(values[word+10]);
        valueID=word+10;//this is a unique id for the word
    }

    value -> setPos(x() - 50*direction, y());
    // adjust position according to collisions
    while(value->collidesWithItem(this)){
        value -> setPos(value->x() - direction, y());
    }
}
