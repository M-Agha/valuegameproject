/**
* \file prestartgamepage.cpp
* \brief implementation for the level selection page
* last page before game start
*
* \author Samir Elkhatib
* \author Mohammad Agha
*/

#include "prestartgamepage.h"
#include "loadingpage.h"
#include "game1scene.h"
#include "game2scenestudent.h"
#include "game2scenemanager.h"
#include "game3scene.h"

/**
 * \brief PreStartGamePage::PreStartGamePage
 * initializes the page labels and buttons
 * \param gameNum speicifes ig game 1, 2 or 3
 * \param gameMenu previous menu to return if pressed back
 * \param parent
 */
PreStartGamePage::PreStartGamePage(int gameNum,Game * gameMenu,QWidget *parent) : QWidget(parent)
{
    setWindowTitle("Level Selection Menu");
    gamePage=gameMenu;
    gameID=gameNum;

    // set up layout according to the chosen game
    if(gameID == 1){
        selectLevelLabel=new QLabel("Select Level");
        selectLevelLabel->setAlignment(Qt::AlignCenter);
        level1Button=new QPushButton("Easy");
        level2Button=new QPushButton("Medium");
        level3Button=new QPushButton("Hard");
        backButton=new QPushButton("Back");
        vLayout=new QVBoxLayout();
        setWinLayout();
        setLayout(vLayout);
        connect(level1Button,SIGNAL(clicked()),this,SLOT(startGame1Easy()));
        connect(level2Button,SIGNAL(clicked()),this,SLOT(startGame1Medium()));//laterwill be different functions for different levels
        connect(level3Button,SIGNAL(clicked()),this,SLOT(startGame1Hard()));
    } else if (gameID==2){
        selectLevelLabel=new QLabel("Select Your Character");
        selectLevelLabel->setAlignment(Qt::AlignCenter);
        level1Button=new QPushButton("Student");
        level2Button=new QPushButton("Manager");
        backButton=new QPushButton("Back");
        instructionLabel=new QLabel("Press Spacebar at Any Moment In Game To Leave It");
        instructionLabel->setAlignment(Qt::AlignCenter);
        vLayout=new QVBoxLayout();
        setWinLayout();

        setLayout(vLayout);
        connect(level1Button,SIGNAL(clicked()),this,SLOT(startStudent()));
        connect(level2Button,SIGNAL(clicked()),this,SLOT(startManager()));
    } else if (gameID == 3){
        selectLevelLabel=new QLabel("Select Level");
        selectLevelLabel->setAlignment(Qt::AlignCenter);
        level1Button=new QPushButton("Easy");
        level2Button=new QPushButton("Medium");
        level3Button=new QPushButton("Hard");
        level4Button=new QPushButton("INSANE");
        backButton=new QPushButton("Back");
        instructionLabel=new QLabel("Press Spacebar at Any Moment In Game To Leave It");
        instructionLabel->setAlignment(Qt::AlignCenter);
        vLayout=new QVBoxLayout();
        setWinLayout();       
        setLayout(vLayout);
        connect(level1Button,SIGNAL(clicked()),this,SLOT(startGameEasy()));
        connect(level2Button,SIGNAL(clicked()),this,SLOT(startGameMedium()));//laterwill be different functions for different levels
        connect(level3Button,SIGNAL(clicked()),this,SLOT(startGameHard()));
        connect(level4Button,SIGNAL(clicked()),this,SLOT(startGameInsane()));
    }

    // connect the back button
    connect(backButton, SIGNAL(clicked()),this,SLOT(backToGamePage()));

}

/**
 * \brief PreStartGamePage::setWinLayout
 * unified layout creation for all games
 * sets the elements into position regardless of the game number
 * accounts for speicific game number as well
 */
void PreStartGamePage::setWinLayout(){
    // general elements
    vLayout->addWidget(selectLevelLabel);
    vLayout->addWidget(level1Button);
    vLayout->addItem(new QSpacerItem(220, 10));
    vLayout->addWidget(level2Button);
    vLayout->addItem(new QSpacerItem(220, 10));

    // game specific elements
    if (gameID==1){
        vLayout->addWidget(level3Button);
        vLayout->addItem(new QSpacerItem(220, 10));
    } else if (gameID == 2){
        vLayout->addWidget(instructionLabel);
        vLayout->addItem(new QSpacerItem(220, 10));
    } else if (gameID == 3){
        vLayout->addWidget(level3Button);
        vLayout->addItem(new QSpacerItem(220, 10));
        vLayout->addWidget(level4Button);
        vLayout->addItem(new QSpacerItem(220, 10));
        vLayout->addWidget(instructionLabel);
        vLayout->addItem(new QSpacerItem(220, 10));
    }
    vLayout->addWidget(backButton);

}

/**
 * \brief PreStartGamePage::backToGamePage
 * called upon returning to previous page
 */
void PreStartGamePage::backToGamePage(){
    gamePage->show();
    this->close();
}

/**
 * \brief PreStartGamePage::startGame1Easy
 * slot for game 1 easy mode
 */
void PreStartGamePage::startGame1Easy(){
    game1scene *scene = new game1scene(this, 1);
    startG(scene);
}

/**
 * \brief PreStartGamePage::startGame1Medium
 * slot for game 1 medium mode
 */
void PreStartGamePage::startGame1Medium(){
    game1scene *scene = new game1scene(this, 2);
    startG(scene);
}

/**
 * \brief PreStartGamePage::startGame1Hard
 * slot for game 1 hard mode
 */
void PreStartGamePage::startGame1Hard(){
    game1scene *scene = new game1scene(this, 3);
    startG(scene);
}

/**
 * \brief PreStartGamePage::startStudent
 * slot for game 2 student
 */
void PreStartGamePage::startStudent(){
    game2scenestudent *scene = new game2scenestudent(this);
    startG(scene);
}

/**
 * \brief PreStartGamePage::startManager
 * slot for game 2 manager
 */
void PreStartGamePage::startManager(){
    game2scenemanager *scene = new game2scenemanager(this);
    startG(scene);
}

/**
 * \brief PreStartGamePage::startGameEasy
 * slot for game 3 easy
 */
void PreStartGamePage::startGameEasy(){
    game3scene *scene = new game3scene(this, 1);
    startG(scene);
}

/**
 * \brief PreStartGamePage::startGameMedium
 * slot for game 3 medium
 */
void PreStartGamePage::startGameMedium(){
    game3scene *scene = new game3scene(this, 2);
    startG(scene);
}
/**
 * \brief PreStartGamePage::startGameHard
 * slot for game 3 hard
 */
void PreStartGamePage::startGameHard(){
    game3scene *scene = new game3scene(this, 3);
    startG(scene);
}
/**
 * \brief PreStartGamePage::startGameInsane
 * slot for game 3 insane
 */
void PreStartGamePage::startGameInsane(){
    game3scene *scene = new game3scene(this, 4);
    startG(scene);
}

/**
 * \brief PreStartGamePage::startG
 * generalized view creation and scene setting up for all games
 * \param scene selected scecne
 */
void PreStartGamePage::startG(QGraphicsScene *scene){
    QGraphicsView *view = new QGraphicsView(scene);
    if (gameID==1)
        view->setWindowTitle("Collect The Good Traits!");
    else if (gameID == 2)
        view->setWindowTitle("Choose Wisely");
    else if (gameID == 3)
        view->setWindowTitle("Catch the Stars");
    view -> setFixedSize(908,512);
    view ->setHorizontalScrollBarPolicy(((Qt::ScrollBarAlwaysOff)));
    view ->setVerticalScrollBarPolicy(((Qt::ScrollBarAlwaysOff)));
    view -> show();
    this->hide();
}
