/**
* \file student.h
* \brief contains characters class definition for student and manager
*
* This header contains the main implementation of the characters.
*
* \author Samir Elkhatib
*/
#ifndef STUDENT_H
#define STUDENT_H

#include <QObject>
#include <QKeyEvent>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>


class student : public QObject, public QGraphicsPixmapItem
{
public:
    explicit student(double, int, int, QGraphicsScene*, QGraphicsItem*, int, QObject *parent = nullptr);
    void keyPressEvent(QKeyEvent *event); // Called on key pressed to perform movement
    QGraphicsScene * scene; // Parent scene which object belongs to
    QGraphicsItem* road; // Movement restricting road for boundaries detection

    bool restrictedMovement; // Set true if character movement restricted to road
    int increment; // increment: character speed
    int Ctype; // Character type: 0 for student, 1 for manager
    double scale; // Character pixmap scaling

    void setMovement(bool);
    void setIncrement(int);
    void setScale(double);
};

#endif // STUDENT_H
