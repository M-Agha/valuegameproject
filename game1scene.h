/**
* \file game1scener.h
* \brief contains pooh character implementation
*
* \author Samir Elkhatib
* \author Mohammad Agha
*/

#ifndef GAME1SCENE_H
#define GAME1SCENE_H

#include <QGraphicsScene>
#include <QTimer>
#include <stdlib.h>
#include "pooh.h"
#include <QGraphicsItem>
#include <QPushButton>
#include "prestartgamepage.h"

class game1scene : public QGraphicsScene
{
    Q_OBJECT
public:
    game1scene(PreStartGamePage *, int); // scene constructor
    QGraphicsTextItem * livesText; // text for lives remaining
    QGraphicsTextItem *gameTimer; // game timer display
    QGraphicsTextItem *scoringText; //score display
    QGraphicsTextItem *moreLivesText; //text if you need more lives to win
    QGraphicsPixmapItem *hearts[10]; // hearts objects representing lives
    pooh *player; // pooh character
    PreStartGamePage *prevPage; // reference to prevoius page
    int level; // hard level
    int time; // time remaining
    int lives; // lives remaining
    int count; // counter for time
    int score; // game score
    QString values[20]={"Brave", "Smart", "Cooperative", "Caring", "Dedicated",
        "Forgiving", "Friendly", "Honest", "Patient", "Respectful",
        "Selfish", "Gossiper", "Lazy", "Dishonest", "Arrogant",
        "Coward", "Greedy", "Aggressive", "Cruel", "Mean"};// the values/traits. It has 10 good values and 10 bad values respectively
    QTimer *timer; // main timer
    QTimer *beesTimer; //bees creation timer
    QFont *newFont; // text font
    void saveToHistory(QString h1, QString h2);
    void initUI();

    /**
     * \brief keyPressEvent
     * Checks when keyboard buttons are clicked
     * \param event
     */
    void keyPressEvent(QKeyEvent *event)
    {
        QGraphicsScene::keyPressEvent(event);
        if (event ->key() == Qt::Key_Space){
            // When spacebar pressed delete the current view and scene and head back to menu
            QGraphicsView *vi = views().at(0);
            vi->hide();
            delete vi;
            prevPage->show();
            delete this;
        }
    }

signals:
    void toBeeTimer();
public slots:
    void createBee();
    void decrementTimer();
    void updateLives(bool s);
    void gameEnd();
    void checkEnd();
};

#endif // GAME1SCENE_H
