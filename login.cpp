/**
* \file login.h
* \brief login page setup
*
* This class is used to setup the widgets for logining in
*
* \author Samir Elkhatib
* \author Mohammad Agha
*/
#include "login.h"
#include "registerwidget.h"
#include "account.h"
/**
 * \brief login::login
 * Sets the page of the login
 * buttons, line edits, label
 * Puts in  a vertical layout and grid layout
 */
login::login(QWidget *parent) : QWidget(parent)
{
    setWindowTitle("Welcome!");
    title = new QLabel("Our Game!");
    title->setAlignment(Qt::AlignCenter);
    username_label = new QLabel("Username:");
    password_label = new QLabel("Password:");
    register_label = new QLabel("Don't have an account?");
    register_label->setAlignment(Qt::AlignCenter);
    username = new QLineEdit();
    password = new QLineEdit();
    password -> setEchoMode(QLineEdit::Password);
    loginBtn = new QPushButton("Login");
    reg = new QPushButton("Create a New Account");
    guest = new QPushButton("Play as a Guest");
    quit = new QPushButton("Quit");
    VLayout = new QVBoxLayout();
    GLayout = new QGridLayout();

    createLayout();
    setLayout(VLayout);

    connect(quit, SIGNAL(clicked()), this, SLOT(close()));
    connect(reg, SIGNAL(clicked()), this, SLOT(openReg()));
    connect(loginBtn, SIGNAL(clicked()), this, SLOT(validate()));
    connect(guest, SIGNAL(clicked()), this, SLOT(openGuest()));
}
/**
 * \brief login::createLayout
 * Add the widgets to the layouts
 */
void login::createLayout(){
    GLayout ->addWidget(username_label, 0, 0);
    GLayout ->addWidget(username, 0, 1);
    GLayout ->addWidget(password_label, 1, 0);
    GLayout ->addWidget(password, 1, 1);

    VLayout ->addWidget(title);
    VLayout ->addItem(GLayout);
    VLayout ->addWidget(loginBtn);
    VLayout ->addWidget(register_label);
    VLayout ->addWidget(reg);
    VLayout->addItem(new QSpacerItem(220, 10));

    VLayout ->addWidget(guest);
    VLayout->addItem(new QSpacerItem(220, 10));

    VLayout ->addWidget(quit);
}
/**
 * \brief login::openReg
 * opens the registration page
 */
void login::openReg(){
    registerWidget *r = new registerWidget(this);
    r -> show();
    this -> hide();
}
/**
 * \brief login::validate
 * searches for a file with the entered user name and validats the pass in the file
 */
void login::validate()
{
    QString pass;
    QString filePath=QDir::currentPath()+"/Accounts/"+username->text()+".txt";
    if(!QFileInfo(filePath).exists()){
        QMessageBox::warning(this,"Login","Invalid Usename. Please try again.");
        return;
    }
    QFile file(filePath);
    if(!file.open(QIODevice::ReadOnly)){
        qDebug()<<"Could not open file "+QDir::currentPath()+"/Accounts/"+username->text()+".txt";
        return;
    }
    QDataStream in(&file);
    in.setVersion(QDataStream::Qt_5_9);
    in>>pass;
    if(pass!=password->text()){
        QMessageBox::warning(this,"Login","Invalid password.Try again.");
        return;
    }
    file.close();
    openAccount();
}

/**
 * \brief login::openAccount
 * opens the accout page after validating
 */
void login::openAccount(){
    Account *a = new Account(this -> username -> text(),this);
    a -> show();
    this -> hide();
}
/**
 * \brief login::openGuest
 * opens the guest page
 */
void login::openGuest(){
    Account *a = new Account("Guest",this);
    a -> show();
    this -> hide();
}
