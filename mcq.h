/**
* \file mcq.cpp
* \brief contains multiple choice questions main implementation
*
* This class contains the main implementation of the MCQs
*
* \author Mohammad Agha
*/
#ifndef MCQ_H
#define MCQ_H

//#include <QObject>
#include <QGraphicsTextItem>
#include <QGraphicsPixmapItem>
#include <QString>
#include <QGraphicsScene>
#include "customqgraphicstextitem.h"
#include <QKeyEvent>

class mcq: public QObject
{
    Q_OBJECT
public:
    mcq(bool,QGraphicsScene *,QString , QString,QString,Qt::GlobalColor,Qt::GlobalColor,int*,int*,int*,int,int*,int*,int,int);
    Qt::GlobalColor color;
    bool questionOnly=false;
    int currentScene;
    int *playerValues;
    int *valueIdA;
    int *valueChangeA;
    int sizeA;
    int *valueIdB;
    int *valueChangeB;
    int sizeB;
    CustomQGraphicsTextItem *tQuestion, *tAnswerA,*tAnswerB;
    void deleteMcq();
    //void keyPressEvent(QKeyEvent *event);
private:
    QGraphicsScene *scene;
    void createTexts();
    //void commitResult(bool);
signals:
    void endMcq(int);
    void setNextScene(int);
public slots:
    void commitResult(int);
};

#endif // MCQ_H
