/**
* \file game3scene.cpp
* \brief contains student game scene implementation
*
* This class contains the main implementation of game 3 scene.
* It constructs the new scene and creates all relevant elements
*
* \author Mohamad Agha
* \author Samir Elkhatib
*/
#include "game3scene.h"
#include "moving_object.h"
/**
 * \brief game3scene::game3scene
 * Scene constructor.
 * Creates elements and starts timers
 * \param prev
 */
game3scene::game3scene(PreStartGamePage *prev, int level)
{
    prevPage=prev;
    this->level = level;

    init();
    connect(timer,SIGNAL(timeout()),this,SLOT(spawnMovingObjects()));
    // Star spawning rate depends on game level
    if(level == 1){
        timer->start(550);
    } else if (level == 2){
        timer->start(450);
    } else if (level == 3){
        timer->start(350);
    } else if (level == 4){
        timer->start(300);
    }


    timer2 = new QTimer();
    connect(timer2, SIGNAL(timeout()), this, SLOT(update()));
    timer2->start(1000/60);
}
/**
*\brief Function that initializes the scene like setting the image and creating the items
*/
void game3scene::init()
{
    setBackgroundBrush(QBrush(QImage(":/images/game3/BG.png").scaled(908, 512)));
    setSceneRect(0,0,908,512);
    setStickyFocus(true);
    timer = new QTimer(this);


    bg = new game3bg(this);
    chr=new character(this);

    time = 0;
    counter = 0;
    score = 0;

    timerText = new QGraphicsTextItem();
    timerText->setPlainText("Time left: "+QString::number(60 - time)+" seconds");
    timerText -> setPos(width() - 160, 30);
    addItem(timerText);
    scoreText = new QGraphicsTextItem();
    scoreText->setPlainText("Score: "+QString::number(score));
    scoreText -> setPos(width() - 160, 45);
    addItem(scoreText);
}
/**
*\brief Function that creates new moving object on two different lanes based on high bool
*/
void game3scene::spawnMovingObjects()
{
    static int count=0;
    count++;
    if(count%2==0)
        new Moving_Object(false,this);
    if(count%2==1&&count!=1)
        new Moving_Object(true,this);

}
/**
*\brief Function that updates the background image of the scene
*/
void game3scene::update(){
    if (level == 4){
        bg->move(7);
    } else{
        bg->move(Bachground_Speed);
    }

    counter ++;
    if (counter == 60){
        counter = 0;
        time++;
        timerText->setPlainText("Time left: "+QString::number(60 - time)+" seconds");
    }
    if (time == 60){
        gameEnd();
    }
    scoreText->setPlainText("Score: "+QString::number(score));
}


/**
 * \brief game3scene::gameEnd
 * Displays all game end results
 * Constructs end scene from given values collected through the game
 * End scene is displayed as an overlay to the current game scene
 * The function requires a global "values" array from which it takes
 * the collected values
 */
void game3scene::gameEnd()
{
    // Overlaying grey rectangle
    QGraphicsRectItem *endBG = new QGraphicsRectItem();
    endBG->setRect(0,0,width(),height());
    endBG->setBrush(QBrush(QColor(100, 100, 100, 200)));
    addItem(endBG);

    // Font element
    QFont titleFont;
    titleFont.setPixelSize(30);

    // Set title according to player's result
    QGraphicsTextItem *endText = new QGraphicsTextItem();
    endText->setDefaultTextColor(Qt::white);
    endText->setFont(titleFont);
    // Stop the timer and detect if player on time to display appropriate text
    disconnect(timer, SIGNAL(timeout()), this, SLOT(spawnMovingObjects()));
    disconnect(timer2, SIGNAL(timeout()), this, SLOT(update()));
    if(score > 0)
        endText->setPlainText("You won!!");
    else if(score<=0)
        endText->setPlainText("Game Over");
    endText->setPos((width()-endText->boundingRect().width())/2, height()/10);
    addItem(endText);

    // Set title according to player's result
    QGraphicsTextItem *endScore = new QGraphicsTextItem();
    endScore->setDefaultTextColor(Qt::white);
    endScore->setFont(titleFont);
    endScore->setPlainText("Your score is: "+QString::number(score));
    endScore->setPos((width()-endScore->boundingRect().width())/2, height()/10 + 50);
    addItem(endScore);

    QString goodV = "";
    QString badV = "";
    QString h1="";
    QString h2="";
    // Initialize the good values string
    for(int i = 0; i < 10; i++){
        if (chr->collectedTraits[i] > 0){
            goodV = goodV + values[i] + ": " +  QString::number(chr->collectedTraits[i]) + " times\n";
            h1+=values[i]+" ";
        }
    }
    if (goodV == "") goodV = "No Good Values \n :(";

    // Initialize the bad values string
    for(int i = 10; i < 20; i++){
        if (chr->collectedTraits[i] > 0){
            badV = badV + values[i] + ": " +  QString::number(chr->collectedTraits[i]) + " times\n";
            h2+=values[i]+" ";
        }
    }
    if (badV == "") badV = "No Bad Values! \n :)";

    //save to History
    saveToHistory(h1,h2);
    // Good values text item
    QGraphicsTextItem *goodText = new QGraphicsTextItem();
    goodText->setPlainText("Good Values Collected: \n" + goodV);
    goodText->setDefaultTextColor(Qt::yellow);
    titleFont.setPixelSize(20);
    goodText->setFont(titleFont);
    goodText->setPos(width()/10, height()*.3 + 10);
    addItem(goodText);

    // Bad values text item
    QGraphicsTextItem *badText = new QGraphicsTextItem();
    badText->setPlainText("Bad Values Collected: \n" + badV);
    badText->setDefaultTextColor(Qt::red);
    titleFont.setPixelSize(20);
    badText->setFont(titleFont);
    badText->setPos(width() - width()/10 - badText->boundingRect().width(),
                      height()*.3+ 10);
    addItem(badText);

    // Back to main menu text
    QGraphicsTextItem *backT = new QGraphicsTextItem();
    backT->setPlainText("Press Spacebar to Go Back To The Menu");
    titleFont.setPixelSize(20);
    backT->setFont(titleFont);
    backT->setDefaultTextColor(Qt::white);
    backT->setPos(width()/2 - backT->boundingRect().width()/2, height()-100);
    addItem(backT);
}
/**
 * \brief game3scene::saveToHistory
 * This fucntion creates a history file if it doesnt exist
 * writes to it or appends  the score related data to be used later
 * \param h1 string that holds the good values collected by the player
 * \param h2 string that holds the bad values collected by the player
 */
void game3scene::saveToHistory(QString h1, QString h2)
{
    QString username=prevPage->gamePage->accountPage->nameLabel->text();
    QString filePath=QDir::currentPath()+"/History/"+username+".txt";
    if(!QDir("History").exists()){
        QDir().mkdir("History");
    }
    QFile file(filePath);
    if(QFileInfo(filePath).exists()){
        if(!file.open(QIODevice::Append)){
            qDebug()<<"Could not open file "+filePath;
            return;
        }
    }
    else{
        if(!file.open(QIODevice::WriteOnly)){
            qDebug()<<"Could not open file "+filePath;
            return;
        }
    }
    QDataStream out(&file);
    out.setVersion(QDataStream::Qt_5_9);
    out<<QString("game3");
    out<<score;
    out<<h1;
    out<<h2;

    file.flush();
    file.close();
}


