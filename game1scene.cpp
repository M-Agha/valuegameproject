/**
* \file game1scene.cpp
* \brief game scene for game 1
*
* This class contains all the implementation of the game scene including pooh and the bees
*
* \author Samir Elkhatib
* \author Mohammad Agha
*/

#include "game1scene.h"
#include "bee.h"
#include "qgraphicsproxywidget.h"

/**
 * \brief game1scene::game1scene
 * \param prev gives reference to the previos panel to allow going back to it
 * \param level specifies how hard the game is
 */
game1scene::game1scene(PreStartGamePage *prev, int level){
    prevPage=prev;
    this -> level = level;

    // set time for the game
    time=120;
    count=0;

    // start the score from time value and decrement
    score = time;
    lives=2;


    // Set game background
    setBackgroundBrush(QBrush(QImage(":/images/bg.PNG")));
    // Set window size
    setSceneRect(0,0,908,512);

    // Create the player
    player = new pooh(this);
    addItem(player);

    // Initialize User Interface
    initUI();


    // Timer for bees creation
    timer = new QTimer(this);
    beesTimer = new QTimer(this);
    connect(beesTimer, SIGNAL(timeout()), this, SLOT(createBee()));
    connect(timer, SIGNAL(timeout()), this, SLOT(decrementTimer()));
    connect(player,SIGNAL(iWon()),this,SLOT(checkEnd()));

    // Create bees every 400 ms with a levels multiplier
    beesTimer -> start(400 - level*100);
    // timer every half a second
    timer -> start (500);

}
/**
 * \brief game1scene::saveToHistory
 * This fucntion creates a history file if it doesnt exist
 * writes to it or appends  the score related data to be used later
 * \param h1 string that holds the good values collected by the player
 * \param h2 string that holds the bad values collected by the player
 */
void game1scene::saveToHistory(QString h1, QString h2)
{
    QString username=prevPage->gamePage->accountPage->nameLabel->text();
    QString filePath=QDir::currentPath()+"/History/"+username+".txt";
    if(!QDir("History").exists()){
        QDir().mkdir("History");
    }
    QFile file(filePath);
    if(QFileInfo(filePath).exists()){
        if(!file.open(QIODevice::Append)){
            qDebug()<<"Could not open file "+filePath;
            return;
        }
    }
    else{
        if(!file.open(QIODevice::WriteOnly)){
            qDebug()<<"Could not open file "+filePath;
            return;
        }
    }
    QDataStream out(&file);
    out.setVersion(QDataStream::Qt_5_9);
    out<<QString("game1");
    out<<score;
    out<<h1;
    out<<h2;
    out<<lives;
    out<<time;

    file.flush();
    file.close();
}

/**
 * \brief game1scene::createBee
 * creates a bee on a random lane and adds it to the scene
 */
void game1scene::createBee(){

    int random =  rand() %8;
    bee *b = new bee(values,this,random);
    addItem(b);
    addItem(b->value);
}


/**
 * \brief game1scene::decrementTimer
 * Uses the existent timer (used to create bees) to create the timer count down processes
 * Also used to decrement the score and change it accordingly
 */
void game1scene::decrementTimer(){
    count++;//this variable is used to make sure a second has passed by incrementing on half a second
    if(count==2){
        time--;
        if(score > 0) score--;
        scoringText->setPlainText("Score: " + QString::number(score));
        gameTimer->setPlainText("Time: "+QString::number(time/60,10)+":"+QString::number(time%60,10));
        if(time==0){
            gameEnd();
        }
        count=0;
    }


}


/**
 * \brief game1scene::gameEnd
 * Displays all game end results
 * Constructs end scene from given values collected through the game
 * End scene is displayed as an overlay to the current game scene
 * The function requires a global "values" array from which it takes
 * the collected values
 */
void game1scene::gameEnd(){

    // Disable player
    player->setEnabled(false);
    timer->stop();
    emit toBeeTimer();

    // Overlaying grey rectangle
    QGraphicsRectItem *endBG = new QGraphicsRectItem();
    endBG->setRect(0,0,width(),height());
    endBG->setBrush(QBrush(QColor(100, 100, 100, 200)));
    addItem(endBG);

    // Title
    QFont titleFont;
    titleFont.setPixelSize(30);
    QGraphicsTextItem *endText = new QGraphicsTextItem();
    endText->setDefaultTextColor(Qt::white);
    endText->setFont(titleFont);

    // Change display title according to whether or not the player won
    if(player->win){
        endText->setPlainText("You Won!");
    } else{
        if (lives == 0) endText->setPlainText("Game Over \n You Lost All Your Lives");
        else endText->setPlainText("Game Over \n Time is UP!");
    }
    endText->setPos((width()-endText->boundingRect().width())/2, height()/10);
    addItem(endText);


    //Score display
    QGraphicsTextItem *scoreText = new QGraphicsTextItem();
    scoreText->setPlainText("Your Score: " + QString::number(score));
    scoreText->setDefaultTextColor(Qt::white);
    titleFont.setPixelSize(20);
    scoreText->setFont(titleFont);
    scoreText->setPos((width()-scoreText->boundingRect().width())/2,
                      (endText->y() + endText->boundingRect().height() + 10));
    addItem(scoreText);

    // Computing values and results for good and for bad values
    QString goodV = "";
    QString badV = "";
    QString h1="";
    QString h2="";
    for(int i = 0; i < 10; i++){
        if (player->collectedTraits[i] > 0){
            goodV = goodV + values[i] + ": " +  QString::number(player->collectedTraits[i]) + " times\n";
            h1+=values[i]+" ";
        }
    }
    if (goodV == "") goodV = "No Good Values \n :(";

    for(int i = 10; i < 20; i++){
        if (player->collectedTraits[i] > 0){
            badV = badV + values[i] + ": " +  QString::number(player->collectedTraits[i]) + " times\n";
            h2+=values[i]+" ";
        }
    }
    if (badV == "") badV = "No Bad Values! \n :)";

    //save to History
    saveToHistory(h1,h2);
    // Good values
    QGraphicsTextItem *goodText = new QGraphicsTextItem();
    goodText->setPlainText("Good Values Collected: \n" + goodV);
    goodText->setDefaultTextColor(Qt::yellow);
    titleFont.setPixelSize(20);
    goodText->setFont(titleFont);
    goodText->setPos(width()/10, scoreText->y() + scoreText->boundingRect().height() + 10);
    addItem(goodText);

    // Bad values
    QGraphicsTextItem *badText = new QGraphicsTextItem();
    badText->setPlainText("Bad Values Collected: \n" + badV);
    badText->setDefaultTextColor(Qt::red);
    titleFont.setPixelSize(20);
    badText->setFont(titleFont);
    badText->setPos(width() - width()/10 - badText->boundingRect().width(),
                     scoreText->y() + scoreText->boundingRect().height() + 10);
    addItem(badText);


    // Back to main menu text
    QGraphicsTextItem *backT = new QGraphicsTextItem();
    backT->setPlainText("Press Spacebar to Go Back To The Menu");
    titleFont.setPixelSize(20);
    backT->setFont(titleFont);
    backT->setDefaultTextColor(Qt::white);
    backT->setPos(width()/2 - backT->boundingRect().width()/2, height()-100);
    addItem(backT);
}


/**
 * \brief game1scene::initUI
 * initializes the interface of the game scene
 * sets text to all UI elements and creates them
 */
void game1scene::initUI(){
    //setting the font of text to make it bold
    newFont=new QFont();
    newFont->setBold(true);

    // Create and display timer
    gameTimer =new QGraphicsTextItem("Time: "+QString::number(time/60,10)+":"+QString::number(time%60,10));
    gameTimer->setPos(width()*0.45,height()*0.84);
    gameTimer->setFont(*newFont);
    addItem(gameTimer);

    // Create and display lives
    livesText=new QGraphicsTextItem("Lives:");
    addItem(livesText);

    // Create the hearts according to the number of lives
    for (int i = 0; i < 10; i++){
        hearts[i] = new QGraphicsPixmapItem();
        hearts[i]->setPixmap(QPixmap(":/images/heart.png").scaled(15,15));
        hearts[i]->setPos(i*23 + 45, 5);
        if (i < lives) addItem(hearts[i]);
    }

    // Create the score text
    scoringText = new QGraphicsTextItem("Score: " + QString::number(score));
    scoringText->setPos(width() - scoringText->boundingRect().width() - 5, 0);
    addItem(scoringText);

    // You need more lives text
    moreLivesText = new QGraphicsTextItem("");
    addItem(moreLivesText);

}

/**
 * \brief game1scene::updateLives
 * updates the lives of the player. Removes a heart or adds one
 * \param s sets if lives increased or decreased. True for increase
 */
void game1scene::updateLives(bool s){
    if (s){
        lives++;
        addItem(hearts[lives-1]);
        score = score + 10;
        scoringText->setPlainText("Score: " + QString::number(score));
    } else{
        lives--;
        removeItem(hearts[lives]);
        score = score - 15;
        if(score < 0) score = 0;
        scoringText->setPlainText("Score: " + QString::number(score));
        if (lives == 0){
            player->win = false;
            gameEnd();
        } else
            player->setPos(width()*0.5,height()*0.01);
    }
}

/**
 * \brief game1scene::checkEnd
 * Checks on game end whether or not the player can proceed to end scene
 */
void game1scene::checkEnd(){
    if (lives >= 4){
        player->win = true;
        gameEnd();
    }else{
        // if number of lives isn't sufficient, ask player to collect more
        moreLivesText->setPlainText("You need at least 4 hearts to win");
        moreLivesText->setPos(width()/2 - moreLivesText->boundingRect().width()/2, 0);
    }
}
