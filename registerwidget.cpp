/**
* \file registerwidget.h
* \brief registerWidget page setup
*
* This class is used to setup the widgets for registration
*
* \author Samir Elkhatib
*/
#include "registerwidget.h"
#include "login.h"
/**
 * \brief registerWidget::registerWidge
 * Sets the page of the registraion
 * buttons, line edits, label
 * Puts in  a vertical layout and grid layout
 */
registerWidget::registerWidget(login* previousPage,QWidget *parent) : QWidget(parent)
{
    setWindowTitle("Sign Up");
    previousMenu=previousPage;
    title = new QLabel("Create a New Account");
    title->setAlignment(Qt::AlignCenter);
    first_name = new QLabel("First Name");
    last_name = new QLabel("Last Name");
    email = new QLabel("Email");
    username = new QLabel("Username");
    password = new QLabel("Password");
    confirm  = new QLabel("Confirm Password");
    age  = new QLabel("Age");
    gender  = new QLabel("Gender");
    first_name_edit = new QLineEdit();
    last_name_edit  = new QLineEdit();
    email_edit  = new QLineEdit();
    username_edit  = new QLineEdit();
    password_edit  = new QLineEdit();
    password_edit -> setEchoMode(QLineEdit::Password);
    confirm_edit  = new QLineEdit();
    confirm_edit -> setEchoMode(QLineEdit::Password);
    age_box = new QSpinBox();
    male = new QRadioButton("Male");
    female = new QRadioButton("Female");
    reset = new QPushButton("Reset");
    submit = new QPushButton("Sign up");
    back = new QPushButton("Back");
    gender_box = new QGroupBox();
    gender_layout = new QVBoxLayout();
    VLayout = new QVBoxLayout();
    GLayout = new QGridLayout();

    setGridLayout();
    setVerticalLayout();
    setLayout(VLayout);

    connect(reset, SIGNAL(clicked()), this, SLOT(reset_data()));
    connect(back, SIGNAL(clicked()), this, SLOT(backToLogin()));
    connect(submit, SIGNAL(clicked()), this, SLOT(regist()));
}
/**
 * \brief registerWidget::setGridLayout
 * Add the widgets to the layouts
 */
void registerWidget::setGridLayout(){
    GLayout ->addWidget(first_name, 0, 0);
    GLayout ->addWidget(last_name, 0, 1);
    GLayout ->addWidget(first_name_edit, 1, 0);
    GLayout ->addWidget(last_name_edit, 1, 1);
    GLayout ->addWidget(email, 2, 0);
    GLayout ->addWidget(username, 2, 1);
    GLayout ->addWidget(email_edit, 3, 0);
    GLayout ->addWidget(username_edit, 3, 1);
    GLayout ->addWidget(password, 4, 0);
    GLayout ->addWidget(confirm, 4, 1);
    GLayout ->addWidget(password_edit, 5, 0);
    GLayout ->addWidget(confirm_edit, 5, 1);
    GLayout ->addWidget(age, 6, 0);
    GLayout ->addWidget(gender, 6, 1);
    GLayout ->addWidget(age_box, 7, 0);
    gender_layout -> addWidget(male, 0);
    gender_layout -> addWidget(female, 1);
    gender_box -> setLayout(gender_layout);
    GLayout ->addWidget (gender_box, 7, 1);
}
/**
 * \brief registerWidget::setVerticalLayout
 * Add the widgets to the layouts
 */
void registerWidget::setVerticalLayout(){

    VLayout -> addWidget(title);
    VLayout -> addItem(GLayout);
    VLayout -> addWidget(reset);
    VLayout -> addWidget(submit);
    VLayout -> addWidget(back);
}
/**
 * \brief registerWidget::reset_data
 * Clears the data from each text field
 */
void registerWidget::reset_data(){
    first_name_edit -> setText("");
    last_name_edit -> setText("");
    email_edit -> setText("");
    username_edit -> setText("");
    password_edit -> setText("");
    confirm_edit -> setText("");
    age_box -> setValue(0);
    male->setAutoExclusive(false);
    male->setChecked(false);
    male->setAutoExclusive(true);
    female->setAutoExclusive(false);
    female->setChecked(false);
    female->setAutoExclusive(true);

}
/**
 * \brief registerWidget::regist
 * Creates the user account bu writing to a file
 */
void registerWidget::regist(){
    //login *loginWidget = new login();
    QString pass=password_edit->text();
    QString filePath=QDir::currentPath()+"/Accounts/"+username_edit->text()+".txt";
    if(QFileInfo(filePath).exists()){
        QMessageBox::warning(this,"Registering","Username Taken. Try another one");
        return;
    }
    if(pass!=confirm_edit->text()){
        QMessageBox::warning(this,"Registering","Passwords Dont match.Try again.");
        return;
    }
    if(!QDir("Accounts").exists()){
        QDir().mkdir("Accounts");
    }
    QFile file(filePath);
    if(!file.open(QIODevice::WriteOnly)){
        qDebug()<<"Could not open file "+filePath;
        return;
    }
    QDataStream out(&file);
    out.setVersion(QDataStream::Qt_5_9);
    out<<pass<<first_name_edit->text()<<last_name_edit->text();
    out<<email_edit->text();
    file.flush();
    file.close();
    Account *a = new Account(this -> username_edit -> text(),previousMenu);
    a -> show();
    this -> close();
}
/**
 * \brief registerWidget::backToLogin
 * returns to the pevious page
 */
void registerWidget::backToLogin(){
    previousMenu ->show();
    this->close();
}
