/**
* \file game3scene.h
* \brief contains the game 3 scene implementation
*
* \author Mohamad Agha
* \author Samir Elkhatib
*/

#ifndef GAME3SCENE_H
#define GAME3SCENE_H

#include <QGraphicsScene>
#include <QTimer>
#include <QGraphicsItem>
/**
* \file game3scene.h
* \brief Game3Scene Class
*
* This file has game 3 scene related fields and fucntion declaration
*/
#include "prestartgamepage.h"
#include "character.h"
#include "game3bg.h"

#define Bachground_Speed 5
class game3scene : public QGraphicsScene
{
    Q_OBJECT
public:
    game3scene(PreStartGamePage *, int);///<Constructor of the game scene
    PreStartGamePage *prevPage; ///<Pointer to the previous page of the menu
    QString values[20]={"Brave", "Smart", "Cooperative", "Caring", "Dedicated",
        "Forgiving", "Friendly", "Honest", "Patient", "Respectful",
        "Selfish", "Gossiper", "Lazy", "Dishonest", "Arrogant",
        "Coward", "Greedy", "Aggressive", "Cruel", "Mean"};///<Array that contains the possible values sed in this game
    void init();///<Function used to initialze the sceen
    character *chr;///<The charater declaration
    game3bg *bg;///<background declaration
    QTimer *timer2;///<timer used to update the scene's items
    QTimer *timer;///<Timer used to spawn moving objects

    QGraphicsTextItem *timerText; ///Displays timer in top right of screen
    QGraphicsTextItem *scoreText; ///Displays score in top right of screen

    int counter; // increments to count one second
    int time; // meassures passed time
    int score; // player score
    int level; // game level: 1- easy, 2- medium, 3- hard

    void gameEnd();
    void saveToHistory(QString h1, QString h2);
signals:

public slots:
    void update();///<Fucntion used to update items on the scene like background
    void spawnMovingObjects();///<Funciton used to spawn moving object triggered by the timer

protected:
    /**
     * \brief keyPressEvent
     * Checks when space button is clicked
     * \param event
     */
    void keyPressEvent(QKeyEvent *event)
    {
        QGraphicsScene::keyPressEvent(event);
        if (event ->key() == Qt::Key_Space){
            // When spacebar pressed delete the current view and scene and head back to menu
            QGraphicsView *vi = views().at(0);
            vi->hide();
            delete vi;
            prevPage->show();
            delete this;
        }

    }
};

#endif // GAME3SCENE_H
