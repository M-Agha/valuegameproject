/**
* \file game.cpp
* \brief game page setup
*
* \author Mohamad Agha
*/
#include "game.h"
#include "prestartgamepage.h"
#include "loadingpage.h"
/**
 * \brief Game::Game
 * Sets the page of the game menu
 * buttons, line edits, label
 * Puts in  a vertical layout and grid layout
 * Includes the description of each gamedisplayed based on player's game choice
 * \param gameName used to indicate which game was chosen
 * \param Account pointer to previous page
 */
Game::Game(QString gameName,Account *accountMenu,QWidget *parent) : QWidget(parent)
{
    setWindowTitle("Start Menu");
    QString Des="Not yet defined";
    if(gameName=="Game 1"){
        gameID=1;
        Des="Our game works as Follow:"
            "\n    •You use the Arrow Keys to move Pooh."
            "\n    •Your Ultimate goal is to reach the honey below with a High Score"
            "\n    •This includes a minimum of 4 hearts"
            "\n    •You can increase the scores by:"
            "\n\t•Reaching there in the least amount of time"
            "\n\t•Higher Number of lives"
            "\n\t•Catching Good values"
            "\n    •Catching Good Values also add Lives"
            "\n    •While catching Bad values makes you lose lives and you have to start over"
            "\n    •You can lose if you have no lives or a one minute timeout";
    }
    else if(gameName=="Game 2"){
        gameID=2;
        Des="Our game works as follows:"
            "\n    •First choose you character: the Student or the Manager."
            "\n    •You use the Arrow Keys to move your character or the vehicle he is riding."
            "\n    •Your goal is to complete the situations by picking the choice you think is best."
            "\n    •Complete the situations within minimal time";
    }
    else if (gameName == "Game 3"){
        gameID=3;
        Des="Our game works as follows:"
            "\n    •Patrick the star is trying to collect yellow stars with positive values"
            "\n    •Help Patrick collect the most number of positive stars while avoiding negative ones"
            "\n    •Use the arrows to go left and right, and the up arrow to jump"
            "\n    •Collect the most values in one minute!";
    }
    gameLabel=new QLabel(gameName);
    gameLabel->setAlignment(Qt::AlignCenter);

    description=new QLabel(Des);

    accountPage=accountMenu;

    newGameButton=new QPushButton("Start New Game");
    resumeGameButton=new QPushButton("Resume Previous Game");
    backButton=new QPushButton("Back");
    hLayout= new QHBoxLayout();
    vLayout=new QVBoxLayout();
    radioBoxLayout=new QVBoxLayout();

    setWinLayout();
    setLayout(hLayout);

    connect(backButton, SIGNAL(clicked()),this,SLOT(backToAccount()));
    connect(newGameButton,SIGNAL(clicked()),this,SLOT(newGamePage()));
    connect(resumeGameButton,SIGNAL(clicked()),this,SLOT(resumeGamePage()));
}
/**
 * \brief Game::setWinLayout
 * Add the widgets to the layouts
 */
void Game::setWinLayout(){

    vLayout->addWidget(gameLabel);
    vLayout->addWidget(newGameButton);
    vLayout->addItem(new QSpacerItem(220, 10));
    vLayout->addWidget(resumeGameButton);

    vLayout->addWidget(backButton);
    hLayout->addLayout(vLayout);
    hLayout->addWidget(description);

}
/**
 * \brief Game::backToAccount
 * Go back to previous page
 */
void Game::backToAccount(){
    accountPage->show();
    this->close();
}
/**
 * \brief Game::newGamePage
 * Start a new game based on gameId
 */
void Game::newGamePage(){
    PreStartGamePage *page =new PreStartGamePage(gameID,this);
    this->hide();
    page->show();
}
void Game::resumeGamePage(){
    //LoadingPage *loadingPage=new LoadingPage(this,"Resuming Game");
    //loadingPage->show();
    //this->hide();
}
