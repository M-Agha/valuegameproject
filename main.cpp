/**
 * \mainpage EECE435L Game
 *
 * This is a project created to remind people of the importance of values.
 * The application uses the modern way of game plays to showcase the values.
 * Our project includes three games
 *
 * \author Mohammad Agha
 * \author Samir Elkhatib
 * \date 10-12-2017
 */

#include <QApplication>
#include <login.h>
#include <QObject>
int main(int argc, char **argv)
{
QApplication app (argc, argv);
// Initiaize and view the login page
login *loginWidget = new login();
loginWidget ->show();

return app.exec();

}
