/**
* \file student.cpp
* \brief contains characters class definition for student and manager
*
* This class contains the main implementation of the characters.
* It constructs character and deals with both types (manager and student)
* The class also handles characcter movement and boundaries collision
*
* \author Samir Elkhatib
*/

#include "student.h"

/**
 * \brief student::student
 * Constructs a new character object according to its appropriate type
 * \param scale, multiplier that scales the character image
 * \param x, position x
 * \param y, position y
 * \param gameScene, QGraphicsScene which the object belongs to
 * \param road, character's road which restricts its movement
 * \param type, integer setting 0 for student and 1 for manager
 * \param parent
 */
student::student(double scale, int x, int y, QGraphicsScene * gameScene, QGraphicsItem *road, int type, QObject *parent) : QObject(parent)
{
    increment=10;
    restrictedMovement=true;
    this->scale = scale;

    scene=gameScene;
    this->road = road;
    this->Ctype = type;
    setScale(scale);
    setPos(x,y);
    setFlag(QGraphicsItem::ItemIsFocusable);
    setFocus();
}

/**
 * \brief student::keyPressEvent
 * Manages character movement and detects collision with boundaries
 * Boundaries are based on collision with scene boundaries and with road object
 * Road resembles the object the character can move on
 * On Each key pressed collision is detected with road before actually finalizing the new position
 * If the character doesn't collide with the road, movement isn't applied
 * \param event, key pressed is passed
 */
void student::keyPressEvent(QKeyEvent *event){
    int xp=x(); // current x position
    int yp=y(); // current y position

    double width=(double)scene->width(); // scene width
    double height=(double)scene->height(); // scene height

    if (event -> key() == Qt::Key_Right && xp < width-33*scale){
        setPos(xp + increment +20*scale, yp);
        if(restrictedMovement&&!(road->collidesWithItem(this)))
            setPos(xp, yp);
        else
            setPos(xp + increment, yp);

    } else if (event -> key() == Qt::Key_Left && xp >increment ){
        setPos(xp - increment - 33*scale, yp);
        if(restrictedMovement&&!(road->collidesWithItem(this)))
            setPos(xp, yp);
        else
            setPos(xp - increment, yp);
    }
    else if (event -> key() == Qt::Key_Up && yp > increment){
        setPos(xp, yp -increment);
        if(restrictedMovement&&!(road->collidesWithItem(this)))
            setPos(xp, yp);
        else
            setPos(xp, yp -increment);
    } else if (event -> key() == Qt::Key_Down && yp < height-85*scale){
        setPos(xp , yp+increment + 85*scale);
        if(restrictedMovement&&!(road->collidesWithItem(this)))
            setPos(xp, yp);
        else
            setPos(xp , yp+increment);
    }
}

/**
 * \brief student::setMovement
 * Sets if player movement is restricted to road or not
 * \param m, is restricted boolean
 */
void student::setMovement(bool m){
    restrictedMovement=m;
}

/**
 * \brief student::setIncrement
 * Sets increment speed of player
 * \param x, new Increment
 */
void student::setIncrement(int x){
    increment = x;
}


/**
 * \brief student::setScale
 * Scales the image according to its type and the scale parameter
 * \param scale, image scale multiplier
 */
void student::setScale(double scale){
    if(Ctype==0){
        this->setPixmap(QPixmap(":/images/student/student.png").scaled(33*scale, 85*scale));
    } else if (Ctype == 1){
        this->setPixmap(QPixmap(":/images/manager/manager.png").scaled(33*scale, 85*scale));
    }

    this->scale = scale;
}
