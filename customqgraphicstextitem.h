#ifndef CUSTOMQGRAPHICSTEXTITEM_H
#define CUSTOMQGRAPHICSTEXTITEM_H
#include <QGraphicsTextItem>
#include <qpainter.h>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>
class CustomQGraphicsTextItem : public QGraphicsTextItem
{
    Q_OBJECT
public:
    Qt::GlobalColor color;
    int id=0;
    CustomQGraphicsTextItem(const QString &text,Qt::GlobalColor color,int id) :
        QGraphicsTextItem(text) {
        this->color=color;
        this->id=id;
    }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
        QStyleOptionGraphicsItem* style = const_cast<QStyleOptionGraphicsItem*>(option);
        painter->fillRect(option->rect,color);
        QGraphicsTextItem::paint(painter, style, widget);
    }
signals:
    void endMcq(int id);
protected:
    void mouseReleaseEvent ( QGraphicsSceneMouseEvent * event ){
      if(id<2&&event->button() == Qt::LeftButton)
      {
        qDebug()<<id;
        emit endMcq(id);
      }
    }

};

#endif // CUSTOMQGRAPHICSTEXTITEM_H


