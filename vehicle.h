/**
* \file vehicle.h
* \brief contains vehicles class definition for cars and bike
*
* \author Samir Elkhatib
*/
#ifndef CAR_H
#define CAR_H

#include <QObject>
#include <QKeyEvent>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QTimer>
#include "student.h"

class vehicle : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    explicit vehicle(QGraphicsScene*, int, int, int, double, QGraphicsItem*, QObject *parent = nullptr);
    int increment; // vehicle speed
    int type; // vehicle type
    double scale; // scaling multiplier for pixmap
    void keyPressEvent(QKeyEvent *event);
    QGraphicsScene * scene; // parent scene
    QTimer *timer; // updating timer for collision detection
    QGraphicsItem* road; // Collision boundary road


signals:

public slots:
    void control();
    void setIncrement(int);
    void setScale(double);
    int getType();
};

#endif // CAR_H
