/**
* \file game2scenesmanager.cpp
* \brief contains manager game scene implementation
*
* This class contains the main implementation of the manager's game scene.
* It constructs the new scene and creates all relevant elements
*
* \author Samir Elkhatib
* \author Mohammad Agha
*/

#include "game2scenemanager.h"

/**
 * \brief game2scenemanager::game2scenemanager
 * Scene constructor.
 * Creates elements and starts timer
 * \param prev
 */
game2scenemanager::game2scenemanager(PreStartGamePage *prev)
{
    prevPage=prev;
    mcqDisplayed = false;

    // Set game background
    setBackgroundBrush(QBrush(QImage(":/images/manager/bedroom.png").scaled(908, 512)));

    // Set window size
    setSceneRect(0,0,908,512);

    // Set sticky focus to prevent player from clearing focus upon mouse click
    setStickyFocus(true);
    road = new QGraphicsPixmapItem();
    overlay = new QGraphicsPixmapItem();
    player = new student(1,0, height() - 100, this, road, 1);


    // call initialize function
    init();

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    connect(timer, SIGNAL(timeout()), this, SLOT(decrementTimer()));
    timer->start(15);
}

/**
 * \brief game2scenestudent::gameEnd
 * Displays all game end results
 * Constructs end scene from given values collected through the game
 * End scene is displayed as an overlay to the current game scene
 * The function requires a global "values" array from which it takes
 * the collected values
 */
void game2scenemanager::gameEnd()
{
    // Overlaying grey rectangle
    QGraphicsRectItem *endBG = new QGraphicsRectItem();
    endBG->setRect(0,0,width(),height());
    endBG->setBrush(QBrush(QColor(100, 100, 100, 200)));
    addItem(endBG);

    // Font element
    QFont titleFont;
    titleFont.setPixelSize(30);

    // Set title according to player's result
    QGraphicsTextItem *endText = new QGraphicsTextItem();
    endText->setDefaultTextColor(Qt::white);
    endText->setFont(titleFont);
    // Stop the timer and detect if player on time to display appropriate text
    disconnect(timer, SIGNAL(timeout()), this, SLOT(decrementTimer()));
    if(time==0)
        endText->setPlainText("You are right on time!!");
    else if(time<0)
        endText->setPlainText("You are Late by "+QString::number(-1*time,10)+" minutes!");
    else
        endText->setPlainText("You are early by "+QString::number(time,10)+" minutes!");
    endText->setPos((width()-endText->boundingRect().width())/2, height()/10);
    addItem(endText);


    QString goodV = "";
    QString h="";
    //QString badV = "";
    // Initialize the good values string
    for(int i = 0; i < 20; i++){
        if (playerValues[i] > 0){
            goodV = goodV + values[i] + "\n\t\t";
            h+=values[i]+" ";
        }
    }
    //if (goodV == "") goodV = "No Good Values \n :(";

    // Initialize the bad values string
//    for(int i = 10; i < 20; i++){
//        if (playerValues[i] > 0){
//            badV = badV + values[i] + "\n";
//        }
//    }
//    if (badV == "") badV = "No Bad Values! \n :)";

    //save to History
    saveToHistory(h);
    // Good values text item
    QGraphicsTextItem *goodText = new QGraphicsTextItem();
    goodText->setPlainText("After doing our thorough statistical analysis on your\nanswers, we found you have the following values: \n\t\t" + goodV);
    goodText->setDefaultTextColor(Qt::yellow);
    titleFont.setPixelSize(20);
    goodText->setFont(titleFont);
    goodText->setPos((width()*0.5)-goodText->boundingRect().width()/2, height()*.3);
    addItem(goodText);

//    // Bad values text item
//    QGraphicsTextItem *badText = new QGraphicsTextItem();
//    badText->setPlainText("Bad Values Collected: \n" + badV);
//    badText->setDefaultTextColor(Qt::red);
//    titleFont.setPixelSize(20);
//    badText->setFont(titleFont);
//    badText->setPos(width() - width()/10 - badText->boundingRect().width(),
//                      height()*.6+ 10);
//    addItem(badText);

    // Back to main menu text
    QGraphicsTextItem *backT = new QGraphicsTextItem();
    backT->setPlainText("Press Spacebar to Go Back To The Menu");
    titleFont.setPixelSize(20);
    backT->setFont(titleFont);
    backT->setDefaultTextColor(Qt::white);
    backT->setPos(width()/2 - backT->boundingRect().width()/2, height()-100);
    addItem(backT);

}
/**
 * \brief game2scenemanager::saveToHistory
 * This fucntion creates a history file if it doesnt exist
 * writes to it or appends  the score related data to be used later
 * \param v string that holds the values attibutes to the player
 */
void game2scenemanager::saveToHistory(QString v)
{
    QString username=prevPage->gamePage->accountPage->nameLabel->text();
    QString filePath=QDir::currentPath()+"/History/"+username+".txt";

    if(!QDir("History").exists()){
        QDir().mkdir("History");
    }
    QFile file(filePath);
    if(QFileInfo(filePath).exists()){
        if(!file.open(QIODevice::Append)){
            qDebug()<<"Could not open file "+filePath;
            return;
        }
    }
    else{
        if(!file.open(QIODevice::WriteOnly)){
            qDebug()<<"Could not open file "+filePath;
            return;
        }
    }
    QDataStream out(&file);
    out.setVersion(QDataStream::Qt_5_9);
    out<< QString("game2")<<QString("Manager");
    out<<v<<time;
    file.flush();
    file.close();
}

/**
 * \brief game2scenemanager::init
 * Initializes all scene objects and sets the status to 0
 * Initializers are implemented here to reduce code from constructor
 */
void game2scenemanager::init(){
    road->setPos(0,0);
    overlay->setPos(0,0);
    addItem(road);
    addItem(overlay);
    addItem(player);
    gameTimer =new QGraphicsTextItem("Minutes till office: "+QString::number(60,10));
    gameTimer->setPos(width()*0.05,height()*0.05);
    QFont *newFont=new QFont();
    newFont->setBold(true);
    gameTimer->setFont(*newFont);
    addItem(gameTimer);
    time=60;
    setStatus(0);
}

/**
 * \brief game2scenestudent::decrementTimer
 * Decrements the game timer and sets its text value each second
 * Function is called every 15 ms from timer signal
 * It waits for 67 calls to decrement the timer since it is called every 15 ms
 */
void game2scenemanager::decrementTimer()
{
    int static count=0;
    count++;
    if(count==67){
        time--;
        if(time>=0){
            gameTimer->setPlainText("Minutes till office: "+QString::number(time%60,10));
        }
        count=0;
    }
}

/**
 * \brief game2scenestudent::update
 * Main game engine to detect character collisions an status updates and movement
 * It depends on the status number to implement functionality according to every scene
 */
void game2scenemanager::update(){
    int x[]={1};
    QGraphicsItem *fItem = focusItem(); // get item in focus
    if(pageStatus==1){
        // on status 1 check if character wants to leave room or go to wife
        //static bool beenHere=false;
        student * s= dynamic_cast<student *>(focusItem());
        if(s){
            if(!beenHere[0] && s->collidesWithItem(wife)){
                currentMCQ->deleteMcq();
                // This MCQ is taking me to next scene when I click, it shouldn't.....
                int y[]={11};
                int z[]={5};
                currentMCQ=new mcq(false,this,"Are you sure you want to have breakfast with your wife when you are\n already late? What is more important? Your call.","Yes","No",Qt::white,Qt::black,playerValues,z,x,1,y,x,1,1);
                beenHere[0]=true;
            }else if(!beenHere[0]&&s->x()>width()-10-s->pixmap().width()){
                currentMCQ->commitResult(1);
                setStatus(3);
            }

        }

    }else if (pageStatus == 2){
        // on status 2 ask character to leave room and check if he goes right
        student * s= dynamic_cast<student *>(focusItem());
        if(s){
            if(s->x()>width()-10-s->pixmap().width()){
                setStatus(3);
            }
        }
    }
    else if (pageStatus == 3||pageStatus==4){
        // on status 3 and 4 check if player collides with banana or if he goes to car
        if (player->collidesWithItem(overlay)){
            // the banana is set in the overlay object

            // new mcq ask if he wants banana. If yes set status 4
            overlay->setPixmap(QPixmap(":/images/student/bedroomOverlay.png"));
            currentMCQ->commitResult(0);
            setStatus(4);
        }
        vehicle * v= dynamic_cast<vehicle *>(focusItem());
        //bool static beenHere=false;
        if(v){
            if (pageStatus == 3){
                if(!beenHere[1]){
                    currentMCQ->commitResult(1);
                    // delete question mcq since this means player chose car
                    // mcq commit that he did not pick banana
                    beenHere[1]=true;
                }

            }
            if (v->x() > width()-10-v->pixmap().width()){
                setStatus(5);
            }
        }



    } else if(pageStatus == 5 || pageStatus == 6){
        // on status 5 and 6 check where car is and accordingly display relevant mcqs

        vehicle * v= dynamic_cast<vehicle *>(focusItem());
        //bool static beenHere=false;
        //bool static beenHere2=false;
        //bool static beenHere3=false;
    if(v){
            if (fItem->x() > 130 && fItem->y() > height()/2 && !mcqDisplayed&&!beenHere[2]){
                mcqDisplayed = true;
                int y[]={12};
                int z[]={3};
                int v[]={0};
                currentMCQ=new mcq(true,this,"There is your kids' school. Don't you want\n to check up on them before going to work?!","","",Qt::transparent,Qt::black,playerValues,z,v,1,y,x,1,5);

                // Display MCQ
            } else if (fItem->x() > 300 && fItem->y() > height()/2 - 200 && mcqDisplayed&&!beenHere[2] ){
                mcqDisplayed = false;
                beenHere[2]=true;
                currentMCQ->commitResult(1);
                // Destroy MCQ
            }
            else if(fItem->x() > 130 && fItem->y() < height()*0.3&&!beenHere[2]){
                mcqDisplayed = false;
                beenHere[2]=true;
                currentMCQ->deleteMcq();
                int y[]={12};
                int z[]={3};
                int v[]={0};
                currentMCQ=new mcq(false,this,"So, you decided to check on your kids?","Yes","No",Qt::transparent,Qt::black,playerValues,z,v,1,y,x,1,5);
            }
            if (fItem->x() > 600 && fItem->y() > height()*.4 && !mcqDisplayed&&!beenHere[3]){
                mcqDisplayed = true;
                beenHere[3]=true;
                int y[]={2};
                int z[]={3};
                int v[]={0};
                currentMCQ=new mcq(true,this,"There is an old lady that needs to cross the street. Dont you want to help her?","","",Qt::transparent,Qt::black,playerValues,z,v,1,y,x,1,5);
                // Display MCQ
            }
            else if(fItem->x() > 700&& fItem->y() > height()*.4 &&mcqDisplayed&&!beenHere[4]){
                mcqDisplayed=false;
                beenHere[4]=true;
                currentMCQ->commitResult(1);
            }
            else if(fItem->x() > 600&& fItem->y() < height()*0.3 &&mcqDisplayed&&!beenHere[4]&&beenHere[3]){
                mcqDisplayed=false;
                beenHere[4]=true;
                currentMCQ->deleteMcq();
                int y[]={2};
                int z[]={3};
                int v[]={0};
                currentMCQ=new mcq(false,this,"The old lady says, could you help me pass? Thank you!","Yes","No",Qt::transparent,Qt::black,playerValues,z,v,1,y,x,1,5);

            }

            if (fItem -> x() > width()-v->pixmap().width()-10){
                setStatus(7);
            }
        }
    } else if (pageStatus == 7){
        // on status 7 check if character goes to building
        student * s= dynamic_cast<student *>(focusItem());
        if(s){

        if (fItem -> x() > width()/2 + 70){
            currentMCQ->deleteMcq();
            setStatus(8);
        }
        }
    } else if (pageStatus == 8){
        // on status 8 check if player goes to office (down) or to right for meeting room
        student * s= dynamic_cast<student *>(focusItem());
        if(s){
            if(!mcqDisplayed){
                mcqDisplayed=true;
                int y[]={13};
                int z[]={14};
                //int v[]={0};
                currentMCQ=new mcq(true,this,"You just remembered someone is waiting in your office offering a\n suspicious deal(move down). Or join the meeting(move to the right).","","",Qt::white,Qt::black,playerValues,y,x,1,z,x,1,5);
            }
            if (s -> x() > width()-100){
                if(mcqDisplayed){
                    mcqDisplayed=false;
                    currentMCQ->commitResult(1);
                }
                disconnect(timer, SIGNAL(timeout()), this, SLOT(decrementTimer()));
                setStatus(9);
            }

            else if (s ->y() > height()/2 -10){
                            if(mcqDisplayed){
                                mcqDisplayed=false;
                                currentMCQ->deleteMcq();
                            }
                            setStatus(10);
             }
        }
    }
    else if(pageStatus == 11||pageStatus==12){
        // on status 11 or 12 check if player goes to employee, display the relevant question if he does, or check if he goes right to the meeting
            student * s= dynamic_cast<student *>(focusItem());
            //bool static beenHere10=false;
            //bool static beenHere11=false;
            if(s){
                if(!mcqDisplayed){
                    mcqDisplayed=true;
                    int y[]={15};
                    int z[]={16};
                    //int v[]={0};
                    currentMCQ=new mcq(true,this,"There is that employee in the top left who accidently entered wrong\n data to the databse. You should go talk to him!","","",Qt::white,Qt::black,playerValues,z,x,1,y,x,1,11);
                }
                if (s -> x() > width()-200){
                    if(mcqDisplayed){
                        mcqDisplayed=false;
                        currentMCQ->deleteMcq();
                    }
                    disconnect(timer, SIGNAL(timeout()), this, SLOT(decrementTimer()));
                    setStatus(9);
                }
                else if (!beenHere[5]&&s->x() < 150 && s->y() < height()/2 - 150){
                    currentMCQ->deleteMcq();
                    int y[]={15};
                    int z[]={16};
                    beenHere[5]=true;
                    currentMCQ=new mcq(false,this,"This is the guy. What will you do?","Encourage him and till him to be \nmore carefull next time","Fire him to make an example out him",Qt::white,Qt::black,playerValues,z,x,1,y,x,1,11);
                }
                else if(pageStatus==12&&beenHere[5]&&!beenHere[6]){
                    int y[]={9};
                    int z[]={10};
                    //int v[]={0};
                    currentMCQ=new mcq(true,this,"Hurry to the meeting. People are waiting(move to the right)","","",Qt::white,Qt::black,playerValues,z,x,1,y,x,1,11);
                    qDebug()<<"here";
                    beenHere[6]=true;
                }

            }

        }
}

/**
 * \brief game2scenemanager::setStatus
 * Scene initializer for all game scenes
 * On each new call, set player scale and position, scene background image, scene road,
 * scene overlay, and any other neccessary elements for the scene that need to be initialized
 * or deleted from the previous status. Each scene has its own images
 *
 * Status table:
 * Status 00: In bed, alarm movement
 * Status 01: Alarm stopped, character created and asked to go to wife or right
 * Status 02: Complimentary for status 1
 * Status 03: Asked to pick banana or go by car after leaving home
 * Status 04: Complimentary for status 3
 * Status 05: Movement on the road with two options: going to children school and helping old lady
 * Status 06: Complimentary to status 5
 * Status 07: Outside Office Building
 * Status 08: Inside work building, asked to go down to his office for malitious deal, or right to meeting
 * Status 09: Static Image: meeting room, calls game end
 * Status 10: Static Image: Office desk, asked to take deal or refuse
 * Status 11: Complimentary status, goes back to inside the building (hallway)
 *
 *
 * \param newS, new status
 */
void game2scenemanager::setStatus(int newS){
    if (newS == 0){
        road->setPixmap(QPixmap(":/images/manager/bedroomFloor.png").scaled(908, 512));
        overlay->setPixmap(QPixmap(":/images/manager/bedroomOverlay.png").scaled(908, 512));
        player->setPos(-300, -300);
        a=new alarm(this);


        //MCQ asking if he wants to wake up
        int x[]={1};
        int y[]={0};
        int z[]={1};
        //int v[]={0};
        currentMCQ=new mcq(false,this,"You have a meeting and you are late. You should wake up and be a good example\n to your employees or sleep more since you can't be penelized as a manager.","Yes","No",Qt::white,Qt::black,playerValues,z,x,1,y,x,1,0);


    } else if(newS == 1){
        road->setPixmap(QPixmap(":/images/manager/bedroomFloor.png").scaled(908, 512));
        overlay->setPixmap(QPixmap(":/images/manager/bedroomOverlay.png").scaled(908, 512));
        player->setScale(2.5);
        player->setPos(width()/2, height()/2 -100);
        a->stopPosition();
        wife = new QGraphicsPixmapItem();
        wife -> setPixmap(QPixmap(":/images/manager/bedroomWife").scaled(908, 512));
        addItem(wife);
        int x[]={1};
        int y[]={11};
        int z[]={5};
        currentMCQ=new mcq(true,this,"Your wife set up breakfast. Should you join her(Move to her)\n or hurry to the meeting(move to the right)?","","",Qt::white,Qt::black,playerValues,z,x,1,y,x,1,1);
        // MCQ asking if he wants to fix his bed

    } else if (newS == 2){

        // MCQ saying that he has to move right to goout of building

    } else if (newS == 3){
        // remove wife and alarm objects
        removeItem(wife);
        delete wife;
        removeItem(a);
        delete a;
        setBackgroundBrush(QBrush(QImage(":/images/manager/preRoad.png").scaled(908, 512)));
        overlay->setPixmap(QPixmap(":/images/manager/preRoadOverlay.png").scaled(908, 512));
        road->setPixmap(QPixmap(":/images/manager/preRoadRoad.png").scaled(908, 512));
        player->setScale(0.8);
        player->setPos(200, height()/2 -200);

        car = new vehicle(this, 2, 350, height()/2, 0.8, road);
        addItem(car);

        // MCQ saying that he has to choose if he leaves or if he removes banana peel
        int x[]={1};
        int y[]={4};
        int z[]={3};

        currentMCQ=new mcq(true,this,"Is that a banana? Someone might get hurt. Ohh, and there is your fancy car.","","",Qt::white,Qt::black,playerValues,z,x,1,y,x,1,3);


    }else if(newS==4){

    }else if (newS == 5){
        gameTimer->setPos(width()*0.05,height()*0.9);
        setBackgroundBrush(QBrush(QImage(":/images/manager/road.png").scaled(908, 512)));
        overlay->setPixmap(QPixmap(":/images/manager/roadOverlay.png").scaled(908, 512));
        road->setPixmap(QPixmap(":/images/manager/roadRoad.png").scaled(908, 512));
        vehicle * v= dynamic_cast<vehicle *>(focusItem());
        if(v){
            v->setIncrement(6);
            v->setScale(0.4);
            v->setPos(20, height()/2 + 60);
        }

    }
    else if(newS==6){

    }else if (newS==7){
        gameTimer->setPos(width()*0.05,height()*0.05);
        setBackgroundBrush(QBrush(QImage(":/images/manager/office.png").scaled(908, 512)));
        overlay->setPixmap(QPixmap(":/images/student/bedroomOverlay.png"));
        road->setPixmap(QPixmap(":/images/manager/officeRoad.png").scaled(908, 512));

        // delete the car object
        QGraphicsItem * v = focusItem();
        removeItem(v);
        delete(v);
        addItem(player);
        player->setPos(20, height()/2);
        player->setFocus();
        int x[]={1};
        currentMCQ=new mcq(true,this,"Hurry and enter the Building!","","",Qt::white,Qt::black,playerValues,x,x,1,x,x,1,7);
    } else if (newS == 8){
        setBackgroundBrush(QBrush(QImage(":/images/manager/inside.png").scaled(908, 512)));
        overlay->setPixmap(QPixmap(":/images/student/bedroomOverlay.png"));
        road->setPixmap(QPixmap(":/images/manager/insideRoad.png").scaled(908, 512));
        player->setPos(width()/2, height()/2 - 50);
        player -> setScale(3);
        player->setFocus();
    } else if(newS == 9){
        setBackgroundBrush(QBrush(QImage(":/images/manager/meeting.png").scaled(908, 512)));
        overlay->setPixmap(QPixmap(":/images/student/bedroomOverlay.png"));
        road->setPixmap(QPixmap(":/images/student/bedroomOverlay.png"));
        player->setPos(-300, -300);
        gameEnd();
    } else if(newS == 10){
        setBackgroundBrush(QBrush(QImage(":/images/manager/desk.png").scaled(908, 512)));
        overlay->setPixmap(QPixmap(":/images/student/bedroomOverlay.png"));
        road->setPixmap(QPixmap(":/images/student/bedroomOverlay.png"));
        player->setPos(-300, -300);
        int x[]={1};
        int y[]={13};
        int z[]={14};
        //int v[]={0};
        currentMCQ=new mcq(false,this,"This is a bribe from a competing company trying to get information.\n Look at all that money!! Would you consider this deal?","Yes","No",Qt::white,Qt::black,playerValues,y,x,1,z,x,1,10);
        mcqDisplayed=false;
    }else if(newS==11){
        setBackgroundBrush(QBrush(QImage(":/images/manager/inside.png").scaled(908, 512)));
        overlay->setPixmap(QPixmap(":/images/student/bedroomOverlay.png"));
        road->setPixmap(QPixmap(":/images/manager/insideRoad.png").scaled(908, 512));
        player->setPos(width()/2, height()/2 - 50);
        player -> setScale(3);
        player->setFocus();

    }


    pageStatus = newS;
}
