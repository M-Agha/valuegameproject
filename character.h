/**
* \file character.h
* \brief Character Class
*
* This file has character related fields and fucntion declaration
*/
#ifndef CHARACTER_H
#define CHARACTER_H

#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QGraphicsScene>
#include <QKeyEvent>
#include <QTimer>

#define Jump_Height 90
#define Jump_Time 7
#define Move_Speed 3

class character : public QObject,public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    explicit character(QGraphicsScene*,QObject *parent = nullptr);///<Constructor of the character object
    void keyPressEvent(QKeyEvent *event);///<fucntion to handle keys that are pressed
    void keyReleaseEvent(QKeyEvent *event);///<fucntion to handle keys that are released
    QTimer *timer;///<timer to handle jump and fall of the character
    QTimer *timer2;///<timer2 is used to handle the character position update
    QGraphicsScene * scene;///<the scene the characetr is in
    int collectedTraits[20]={0};///<The array that holds the values collected by the character indicated by the valueId as index
    bool inFall=false;///<a boolean used to tell if the character is now falling. It is used to make sure he can't jump when falling
    QSet<int> pressedKeys;///<A set used to hold all the pressed keys at the same time
public slots:
    void jump();///<Function that handles the jump of a character
    void fall();///<Function that handles the falling of a character
    void updatePos();///<Function that updates the player position based on the different keys pressed and stored in the pressedKeys set
signals:
};

#endif // CHARACTER_H
