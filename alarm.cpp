/**
* \file alarm.cpp
* \brief contains alarm object definition
*
* This class contains the main implementation of the alarm and its animation.
*
* \author Mohammad Agha
*/

#include "alarm.h"
//#include "QDebug"
/**
 * \brief alarm::alarm
 * creates an alarm and its timer
 * \param gameScene parent scene
 * \param parent
 */
alarm::alarm(QGraphicsScene * gameScene, QObject *parent): QObject(parent)
{
    scene=gameScene;
    setPos(gameScene->width()*0.015,gameScene->height()*0.005);
    setPixmap(QPixmap(":/images/student/alarmOnly.png").scaled(908, 512));
    //setPos(200,100);
    scene->addItem(this);
    timer =new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(alarmJump()));
    timer->start(100);
}

/**
 * \brief alarm::stopPosition
 * Returns alarm object to its correct position after stopping animation
 */
void alarm::stopPosition()
{
    timer->stop();
    disconnect(timer,SIGNAL(timeout()),this,SLOT(alarmJump()));
    if(upOrDown==0){
        setPos(x(), y()+10);
        upOrDown=1;
    }
}

/**
 * \brief alarm::alarmJump
 * Alarm jumping animation implementation
 */
void alarm::alarmJump()
{
    if(upOrDown==0){
        setPos(x(), y()+10);
        upOrDown=1;
    }
    else{
        setPos(x(), y()-10);
        upOrDown=0;
    }
}
