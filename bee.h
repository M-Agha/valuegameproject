/**
* \file bee.h
* \brief contains implementation of game 1 bees
*
* \author Samir Elkhatib
* \author Mohammad Agha
*/
#ifndef BEE_H
#define BEE_H

#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsTextItem>
#include <QTimer>
#include <QGraphicsScene>
#include <QString>
#include"game1scene.h"

class bee : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    explicit bee(QString[],game1scene *,int lane,QObject *parent = nullptr);
    QTimer *timer; // bee movement timer
    QImage beeImage; // bee image
    QGraphicsTextItem *value; // bee's value text
    int valueID; // id of the value in values list
    int lane; // lane number
    int direction; // direction right or left (-1 or 1)
    game1scene * scen; // parent scene
    double height; // scene height
    double width;// scene width
    void init();
    void createValue(QString[]);
signals:

public slots:
    void move();
    void timerKill();
};

#endif // BEE_H
