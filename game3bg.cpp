/**
* \file game3bg.cpp
* \brief contains game 3 background class implementation
*
* The class depends on two main obects that are doubled to simulate movement
*
* \author Samir Elkhatib
*/
#include "game3bg.h"

/**
 * \brief game3bg::game3bg
 * Two initial objects are crreated and their clones are created as well
 * bg1 and r1 are the initial objects resembling the background and the road
 * bg2 and r2 are the duplicates which immerge into the scene as the initial objects move
 * When the objects are out of bounds, they return to initial position which stimulates infinite movement.
 * \param s reference to the parent scene
 * \param parent
 */
game3bg::game3bg(QGraphicsScene *s, QObject *parent) : QObject(parent)
{
    scene = s;

    bg1 = new QGraphicsPixmapItem();
    bg1->setPixmap(QPixmap(":/images/game3/backRoad.png").scaled(908, 512));
    bg1->setPos(0,0);
    bg2 = new QGraphicsPixmapItem();
    bg2->setPixmap(QPixmap(":/images/game3/backRoad.png").scaled(908, 512));
    bg2->setPos(scene->width(), 0);
    scene->addItem(bg1);
    scene->addItem(bg2);

    r1 = new QGraphicsPixmapItem();
    r1->setPixmap(QPixmap(":/images/game3/road.png").scaled(908,512));
    r1->setPos(0,0);
    r2 = new QGraphicsPixmapItem();
    r2->setPixmap(QPixmap(":/images/game3/road.png").scaled(908,512));
    r2->setPos(scene->width(),0);
    scene->addItem(r1);
    scene->addItem(r2);

}

/**
 * \brief game3bg::move
 * \param increment value taken to change the movement speed
 */
void game3bg::move(int increment){
    int bgFactor = 5;
    // The background moves at a slower pace than the foreground road to stimulate dimension
    bg1->setPos(bg1->x()-increment/bgFactor, 0);
    bg2->setPos(bg2->x()-increment/bgFactor, 0);

    r1->setPos(r1->x()-increment, 0);
    r2->setPos(r2->x()-increment, 0);

    if(bg1->x()<= -scene->width()){
        bg1->setPos(0,0);
        bg2->setPos(scene->width(), 0);
    }

    if(r1->x() <= -scene->width()){
        r1->setPos(0,0);
        r2->setPos(scene->width(),0);
    }
}
