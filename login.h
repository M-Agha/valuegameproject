/**
* \file login.h
* \brief login page setup
*
* \author Samir Elkhatib
* \author Mohammad Agha
*/
#ifndef LOGIN_H
#define LOGIN_H

#include <QWidget>
#include <QtWidgets>
#include <QMessageBox>
/**
* \file login.h
* \brief login Class
*
* This file has login widgets to be displayed
*/
class login : public QWidget
{
    Q_OBJECT
public:
    explicit login(QWidget *parent = nullptr);
    QLabel *title, *username_label, *password_label, *register_label;///<Labels to display text
    QLineEdit *username, *password;///<Fields for users to input their pass and user name
    QPushButton *loginBtn, *reg, *guest, *quit;///<Button to change pages and authenticate
    QGridLayout *GLayout;///<Grid layout to spread the differnt widgets
    QVBoxLayout *VLayout;///<Vertical layout to spread the differnt widgets vertically

    void createLayout();///<Function that adds widgets to layout
    void openAccount();///<Function to go to user account. Next page
signals:

public slots:
    void openReg();///<Function to open registartion page
    void validate();///<Function to validadte credentials
    void openGuest();///<Function to open guest page
};

#endif // LOGIN_H
