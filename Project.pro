QMAKE_CXXFLAGS += -pg
QMAKE_CFLAGS += -pg
OBJECTS_DIR = .\Objects
QT += widgets
QT += charts

SOURCES += \
    main.cpp \
    login.cpp \
    registerwidget.cpp \
    account.cpp \
    loadingpage.cpp \
    prestartgamepage.cpp \
    pooh.cpp \
    mcq.cpp \
    student.cpp \
    vehicle.cpp \
    character.cpp \
    alarm.cpp \
    bee.cpp \
    game.cpp \
    game1scene.cpp \
    game2scenemanager.cpp \
    game2scenestudent.cpp \
    game3scene.cpp \
    homepage.cpp \
    moving_object.cpp \
    game3bg.cpp

HEADERS += \
    login.h \
    registerwidget.h \
    account.h \
    game.h \
    loadingpage.h \
    prestartgamepage.h \
    game1scene.h \
    pooh.h \
    mcq.h \
    customqgraphicstextitem.h \
    student.h \
    game2scenemanager.h \
    game2scenestudent.h \
    game3scene.h \
    character.h \
    alarm.h \
    bee.h \
    homepage.h \
    vehicle.h \
    moving_object.h \
    game3bg.h

RESOURCES += \
    resources.qrc
