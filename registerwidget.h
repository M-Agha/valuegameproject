/**
* \file registerwidget.h
* \brief registerWidget page setup
*
* \author Samir Elkhatib
*/
#ifndef REGISTERWIDGET_H
#define REGISTERWIDGET_H

#include <QWidget>
#include <QtWidgets>
#include "login.h"
#include <QFile>
#include <QFileInfo>
#include "account.h"
/**
* \file login.h
* \brief login Class
*
* This file has login widgets to be displayed
*/
class registerWidget : public QWidget
{
    Q_OBJECT
public:
    explicit registerWidget(login*,QWidget *parent = nullptr);

    login *previousMenu;///<Pointer to the previous page of the menu
    QLabel *title, *first_name, *last_name, *email, *username, *password, *confirm, *age, *gender;///<Labels to display text
    QLineEdit *first_name_edit, *last_name_edit, *email_edit, *username_edit, *password_edit, *confirm_edit;///<Fields for users to input their pass and user name
    QSpinBox *age_box;///<A widget to hold the age 
    QRadioButton *male, *female;///< radio buttons
    QPushButton *reset, *submit, *back;///< buttons
    QVBoxLayout *VLayout;///<Vertical layout to spread the differnt widgets vertically
    QGridLayout *GLayout;///<Grid layout to spread the differnt widgets
    QGroupBox *gender_box;///<widget to hold the radio buttons
    QVBoxLayout *gender_layout;///<box to contain the gender box

    void setVerticalLayout();///<Sets the widgets in the vertical layout
    void setGridLayout();///<Sets the widgets in the grid layout

signals:

public slots:
    void reset_data();///<cleans all the values entered
    void regist();///<registers the new user
    void backToLogin();///<returns to previous page
};

#endif // REGISTERWIDGET_H
