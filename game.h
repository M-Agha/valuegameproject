/**
* \file game.h
* \brief game page setup
*
* \author Mohamad Agha
*/
#ifndef GAME_H
#define GAME_H

#include <QWidget>
#include <QtWidgets>
#include "account.h"

/**
* \file game.h
* \brief game page setup
*
* This file has game setup and widgets to be displayed
*/
class Game : public QWidget
{
    Q_OBJECT
public:
    explicit Game(QString,Account*,QWidget *parent = nullptr);
    Account * accountPage;
    QLabel * gameLabel;
    QLabel * description;
    QPushButton * newGameButton;
    QPushButton * resumeGameButton;
    QPushButton * backButton;
    QVBoxLayout * vLayout;
    QVBoxLayout * radioBoxLayout;
    QHBoxLayout * hLayout;
    int gameID;
    void setWinLayout();
signals:

public slots:
    void backToAccount();
    void newGamePage();
    void resumeGamePage();
};

#endif // GAME_H
