/**
* \file loadingpage.cpp
* \brief contains manager game scene implementation
*
* This class is used to provide history statistics to players with charts on performance
*
* \author Mohammad Agha
*/
#include "loadingpage.h"
/**
 * \brief LoadingPage::LoadingPage
 * Sets the page of the history
 * Puts in  a vertical layout and scroll area
 * \param previousMenu pointer to the previous page
 * \param title string holding the title
 */
LoadingPage::LoadingPage(Account * previousMenu,QString title,QWidget *parent) : QWidget(parent)
{
    setWindowTitle(title);
    previousPage=previousMenu;
    loadingLabel=new QLabel();

    backButton=new QPushButton("Back");
    vLayout =new QVBoxLayout();
    addHistory();
    QScrollArea *scrollArea = new QScrollArea();
    scrollArea-> setWidget(loadingLabel);
    vLayout->addWidget(scrollArea);

    vLayout->addWidget(backButton);

    setLayout(vLayout);
    connect(backButton, SIGNAL(clicked()),this,SLOT(backToPreStartGamePage()));
}
/**
 * \brief LoadingPage::addHistory
 * looks for a file in the history folder that have the same username
 * Opens the history file and reads the first string which contains which game this is
 * On differnt games, it will call different handlers to extarct the info based on the game format
 */
void LoadingPage::addHistory()
{
    QString username=previousPage->nameLabel->text();
    QString filePath=QDir::currentPath()+"/History/"+username+".txt";
    if(!QDir("History").exists()){
        QDir().mkdir("History");
    }
    if(!QFileInfo(filePath).exists()){
        loadingLabel->setText("You haven't played any games yet!");
        return;
    }
    QFile file(filePath);
    if(!file.open(QIODevice::ReadOnly)){
        qDebug()<<"Could not open file "+filePath;
        return;
    }
    QDataStream in(&file);
    in.setVersion(QDataStream::Qt_5_9);
    QString h="";
    int n=0;
    while(!in.atEnd()){

        QString game="";
        in >>game;
        n++;
        if(game==QString("game3")){
            h+="Play Number: "+QString::number(n,10)+"\n"+handleGame3History(&in)+"\n";
        }
        else if(game==QString("game1")){
            h+="Play Number: "+QString::number(n,10)+"\n"+handleGame1History(&in)+"\n";
        }
        else if(game==QString("game2")){
            h+="Play Number: "+QString::number(n,10)+"\n"+handleGame2History(&in)+"\n";
        }
    }
    loadingLabel->setText(h);
    QLineSeries *series1 = new QLineSeries();
    QLineSeries *series3 = new QLineSeries();
    for(int i =0;i<game1Scores.size();i++){
        series1->append(i, game1Scores.at(i));
    }
    for(int i =0;i<game3Scores.size();i++){
        series3->append(i, game3Scores.at(i));
    }

    QChart *chart = new QChart();
    chart->addSeries(series1);
    chart->addSeries(series3);
    chart->createDefaultAxes();
    chart->setTitle("Game 1 and Game 3 Performance\nGame Tries vs Score");
    chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->setFixedSize(1000, 1000);
    vLayout->addWidget(chartView);
    file.close();
}
/**
 * \brief LoadingPage::backToPreStartGamePage
 * Returns to previous page
 */
void LoadingPage::backToPreStartGamePage(){
    ((QWidget*)previousPage)->show();
    this->close();
}
/**
 * \brief LoadingPage::handleGame2History
 * Extracts variables from stream based on game format
 * Returns the string formatted to be vied directly in the label
 * \param in the datastream of the file
 */
QString LoadingPage::handleGame2History(QDataStream *in){
    QString type="";
    QString values="";
    int time;
    QString stime;
    (*in)>>type>>values>>time;
    if(time==0)
        stime="You were right on time";
    else if(time<0)
       stime="You were "+QString::number(-1*time,10)+" minutes late";
    else
        stime="You were "+QString::number(time,10)+" minutes early";
    return "Game Played: Game 2\nPlayed as "+type+"\nValues given: "+values+"\n"+stime+"\n";

}
/**
 * \brief LoadingPage::handleGame3History
 * Extracts variables from stream based on game format
 * Returns the string formatted to be vied directly in the label
 * \param in the datastream of the file
 */
QString LoadingPage::handleGame3History(QDataStream *in)
{
    QString h1="";
    QString h2="";
    int score;
    (*in)>>score>>h1>>h2;
    game3Scores.append(score);
    return "Game Played: Game 3\nScore: "+QString::number(score,10)+
            "\nGood Values Collected: "+h1+
            "\nBad Values Collected: "+h2+"\n";
}
/**
 * \brief LoadingPage::handleGame1History
 * Extracts variables from stream based on game format
 * Returns the string formatted to be vied directly in the label
 * \param in the datastream of the file
 */
QString LoadingPage::handleGame1History(QDataStream *in)
{
    QString h1="";
    QString h2="";
    int score;
    int time;
    int lives;
    (*in)>>score>>h1>>h2>>lives>>time;
    game1Scores.append(score);
    return "Game Played: Game 1\nScore: "+QString::number(score,10)+"\nTime: "+QString::number(time,10)+
            "\nLives: "+QString::number(lives,10)+"\nGood Values Collected: "+h1+
            "\nBad Values Collected: "+h2+"\n";
}
